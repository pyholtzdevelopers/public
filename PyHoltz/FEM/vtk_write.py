#_____________________________________________________________________
# Copyright (c) 2018 Georg Mensah et al.
# All rights reserved.
#
# Last edited by: 
#
# Date: 
#
# This file is part of PyHoltz.
# PyHoltz is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Lesser General Public License as published by 
# the Free Software Foundation, version 3 of the License. 
#
# PyHoltz is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of 
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License 
# (COPYING.LESSER) along with PyHoltz.
# If not, see <https://www.gnu.org/licenses/>.
# ____________________________________________________________________
"""
Created on Mon Aug 21 14:26:33 2017

@author: georg
"""
import numpy as np

def vtk_write(file_name,mesh,data=[]):
      fobj=open(file_name+".vtu","w") 
      fobj.write("<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" >\n")#byte_order=\"BigEndian\"
      fobj.write("\t<UnstructuredGrid>\n")
      fobj.write("\t\t<Piece NumberOfPoints=\""+str(mesh.Points.shape[0])+"\" NumberOfCells=\""+str(len(mesh.Tetrahedra))+"\">\n")
      fobj.write("\t\t\t<Points>\n")
      fobj.write("\t\t\t\t<DataArray type=\"Float64\" NumberOfComponents=\"3\" format=\"ascii\">")
      for pnt in mesh.Points:
         fobj.write(str(pnt[0])+' '+str(pnt[1])+' '+str(pnt[2])+' ')
      
      fobj.write("</DataArray>")
      fobj.write("\t\t\t</Points>\n")
      fobj.write("\t\t\t<Cells>\n")
      fobj.write("\t\t\t\t<DataArray type=\"Int32\" Name=\"connectivity\" format=\"ascii\">")
      for tet in mesh.Tetrahedra:
         for pnt in tet:
            fobj.write(str(pnt)+" ")
         
      
      fobj.write("</DataArray>\n")
      fobj.write("\t\t\t\t<DataArray type=\"Int32\" Name=\"offsets\" format=\"ascii\">")
      off=0
      for tet in mesh.Tetrahedra:
            off+=len(tet)
            fobj.write(str(off)+" ")
      
      fobj.write("</DataArray>\n")
      fobj.write("\t\t\t\t<DataArray type=\"UInt8\" Name=\"types\" format=\"ascii\">")
      for tet in mesh.Tetrahedra:
         for pnt in tet:
            fobj.write("10 ")
         
      
      fobj.write("</DataArray>\n")
      fobj.write("\t\t\t</Cells>\n")

      fobj.write("\t\t\t<PointData>\n")
      for dataname in data.keys():
         fobj.write("\t\t\t\t<DataArray type=\"Float64\" Name=\""+dataname+"\" format=\"ascii\">")
         for datum in data[dataname]:
               fobj.write(str(datum.item(0))+" ")


         
         fobj.write("\n\t\t\t\t</DataArray>\n")
      
      fobj.write("\t\t\t</PointData>\n")
      fobj.write("\t\t</Piece>\n")
      fobj.write("\t</UnstructuredGrid>\n")
      fobj.write("</VTKFile>\n")
   



def vtk_write2(file_name,mesh,data=[]):
      from PyHoltz.FEM.mesh_utils import smplx_finder
      import numpy as np
      fobj=open(file_name+".vtu","w") 
      fobj.write("<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" >\n")#byte_order=\"BigEndian\"
      fobj.write("\t<UnstructuredGrid>\n")
      fobj.write("\t\t<Piece NumberOfPoints=\""+str(mesh.Points.shape[0]+mesh.Lines.shape[0])+"\" NumberOfCells=\""+str(len(mesh.Tetrahedra))+"\">\n")
      fobj.write("\t\t\t<Points>\n")
      fobj.write("\t\t\t\t<DataArray type=\"Float64\" NumberOfComponents=\"3\" format=\"ascii\">")
      for pnt in mesh.Points:
         fobj.write(str(pnt[0])+' '+str(pnt[1])+' '+str(pnt[2])+' ')
      for line in mesh.Lines:
          pnt=np.mean(mesh.Points[line],0)
          fobj.write(str(pnt[0])+' '+str(pnt[1])+' '+str(pnt[2])+' ')
          
      
      fobj.write("</DataArray>")
      fobj.write("\t\t\t</Points>\n")
      fobj.write("\t\t\t<Cells>\n")
      fobj.write("\t\t\t\t<DataArray type=\"Int32\" Name=\"connectivity\" format=\"ascii\">")
      for tet in mesh.Tetrahedra:
         for pnt in tet:
            fobj.write(str(pnt)+" ")
         lines=[]

         for idx in [[0, 1],[1,2],[2,0],[0,3],[3,1],[3,2]]:
             lines.append(smplx_finder(tet[idx],mesh.Lines))
         
         for pnt in lines:
            fobj.write(str(pnt+mesh.Points.shape[0])+" ")
             
         
      
      fobj.write("</DataArray>\n")
      fobj.write("\t\t\t\t<DataArray type=\"Int32\" Name=\"offsets\" format=\"ascii\">")
      off=0
      for tet in mesh.Tetrahedra:
            off+=10
            fobj.write(str(off)+" ")
      
      fobj.write("</DataArray>\n")
      fobj.write("\t\t\t\t<DataArray type=\"UInt8\" Name=\"types\" format=\"ascii\">")
      for tet in mesh.Tetrahedra:
         for pnt in tet:
            fobj.write("24 ")
         
      
      fobj.write("</DataArray>\n")
      fobj.write("\t\t\t</Cells>\n")

      fobj.write("\t\t\t<PointData>\n")
      for dataname in data.keys():
         fobj.write("\t\t\t\t<DataArray type=\"Float64\" Name=\""+dataname+"\" format=\"ascii\">")
         for datum in data[dataname]:
               fobj.write(str(datum.item(0))+" ")


         
         fobj.write("\n\t\t\t\t</DataArray>\n")
      
      fobj.write("\t\t\t</PointData>\n")
      fobj.write("\t\t</Piece>\n")
      fobj.write("\t</UnstructuredGrid>\n")
      fobj.write("</VTKFile>\n")

#%%
def bloxtent(msh,b,p):
    P=np.matrix(np.zeros((p.shape[0]*msh.DoS,p.shape[1]),dtype=complex))
    for k in range(msh.DoS):
        P[msh.blochxmap[k],:]=p*np.exp(-1j*b*2*np.pi/msh.DoS*k)
    return P
    