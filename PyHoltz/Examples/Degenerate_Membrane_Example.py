#_____________________________________________________________________
# Copyright (c) 2018 Georg Mensah et al.
# All rights reserved.
#
# Last edited by: 
#
# Date: 
#
# This file is part of PyHoltz.
# PyHoltz is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Lesser General Public License as published by 
# the Free Software Foundation, version 3 of the License. 
#
# PyHoltz is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of 
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License 
# (COPYING.LESSER) along with PyHoltz.
# If not, see <https://www.gnu.org/licenses/>.
# ____________________________________________________________________

"""
Created on Thu Mar 15 14:57:53 2018
@author: orchini
3-fold degenerate eigenvalue problem in structural mechanics, when considering a simply clamped plate's vibrations
The 3-fold degeneracy is due to symmetry (2 modes) + 1 accidental degeneracy.
The accidental degeneracy is removed at first order, the symmetry induced one at second order.
The Chebychev method used to solve the problem is adapted from Trefethen "Spectral Methods" book
"""
#import os
#os.chdir('../')

import numpy as np
from scipy.linalg import eig
import numpy.matlib as npm
from PyHoltz.NLEVP.all_algebra import pow1, pow2
from PyHoltz.NLEVP.parameter import para_model
from copy import deepcopy
import pickle

def cheb(N):
    if N==0:
        D=0 
        x=1
    else:
        x = np.asarray([np.cos(np.pi*(i)/N) for i in range(N+1)])
              
        c = np.multiply(np.asarray([2] + np.ones(N-1).tolist() + [2]),
                      np.asarray([(-1)**i for i in range(N+1)]))
        X = npm.repmat(x.reshape(N+1,1),1,N+1)
        dX = X-X.transpose()
        D  = ( np.outer(c,1/c) )/(dX+(np.eye(N+1)))      #% off-diagonal entries
        D  = D - np.diag(np.sum(D,1))               #% diagonal entries     
    return D, x

def polyEig(eps,eps0,order,branch,lamps):
    lam = 0.
    for k in range(order+1):
        lam += (eps - eps0)**k*lamps[branch[0:k]]
    return lam
#%%

N = 26
D, x = cheb(N)
# reduce the domain to a size-1 square to have normalised eigenvalues
scale = 2
x = x/scale
# The chebychev matrices are space dependent! Scale them accordingly.
D = D*scale
D2 = D@D

D =  D[1:N,1:N]
D2 = D2[1:N,1:N]

# Apply BC
I = np.eye(N-1)
Dx = np.kron(I,D)
Dy = np.kron(D,I)
L = np.kron(I,D2) + np.kron(D2,I)
               
[xxg,yyg] = np.meshgrid(x[1:N],x[1:N])
xxL = xxg.reshape((N-1)**2,1)
yyL = yyg.reshape((N-1)**2,1)

# Define spatial-dependent perturbations
pert1 = (np.cos(2*np.pi*xxL)+np.cos(2*np.pi*yyL)).flatten()
pert2 = (np.cos(2*np.pi*xxL)*np.cos(1*np.pi*yyL)).flatten()
M1 = np.diag(pert1)
M2 = np.diag(pert2)

L0 = L@L + 0j
L1 = 10*M1 + 0j
L2 = 20*M2 + 0j         

#%% Build the operator
Lm=para_model()

Lm.add_param('ω',1.)
Lm.add_param('eps',0)
Lm.add_param('λ',0)

Lm.add_term({'matrix':np.matrix(L0),'functions':[],'parameters':[],'operator':'L0'})
Lm.add_term({'matrix':np.matrix(-np.eye((N-1)**2)),'functions':[pow1],'parameters':[['ω']],'operator':'-I'})
Lm.add_term({'matrix':np.matrix(L1),'functions':[pow1],'parameters':[['eps']],'operator':'L1'})
Lm.add_term({'matrix':np.matrix(L2),'functions':[pow2],'parameters':[['eps']],'operator':'L2'})
Lm.add_term({'matrix':np.matrix(-np.eye((N-1)**2)),'functions':[pow1],'parameters':[['λ']],'operator':'-λ'})

#%% Build auxiliary operator (this is a linear eigenvalue problem, we can use eig).
M=para_model()

M.add_param('ω',1.)
M.add_param('eps',0)

M.add_term({'matrix':np.matrix(L0),'functions':[],'parameters':[],'operator':'L0'})
M.add_term({'matrix':np.matrix(L1),'functions':[pow1],'parameters':[['eps']],'operator':'L1'})
M.add_term({'matrix':np.matrix(L2),'functions':[pow2],'parameters':[['eps']],'operator':'L2'})
M.active=['eps']

# Calculate exact unperturbed solution using eig
w2, p_adj, p = eig(M(0,0), left=True)
inds = np.argsort(np.abs(w2))
w = np.array(np.mean(w2[inds[30:33]]))
p = np.matrix(p[:,inds[30:33]])
p_adj = np.matrix(p_adj[:,inds[30:33]])
# Compare solution with theoretically known one
m,n=5,5
expected_eigenvalue=np.pi**4*((m)**2/(2/scale)**2+(n)**2/(2/scale)**2)**2
print('Expected eigenvalue:', expected_eigenvalue)
print('Obtained eigenvalue:', w)
print('Absolute error:', abs(expected_eigenvalue-w))

#%% Assign the solution to the nonlinear operator
Lm.set_value('eps',0.0)
Lm.omega = np.real(w) + 0j
Lm.p = np.real(p) + 0j
Lm.p_adj = np.real(p_adj) + 0j

#%% Run perturbation theory
order = 10
Lm.perturb_degenerate('eps', N=order, issparse=False, debug=True, rtol=100, normalisation=False)

pickle.dump(Lm, open("Deg_Res.pickle", "wb" ) )

#%% Calculate exact solution
eps0 = 0
M.active=['eps']
branches = [ i for i in Lm.Result[0].keys() if len(i)==order]
inset = 1
N_sol=101
eps = np.linspace(-inset,inset,N_sol)
    
w_Exact = []
p_Exact = []
inds = [0,1,2]

for n, ep in enumerate(eps):
    if not n%10:
        print('Calculating solution',n+1,'of',N_sol)
    w, p = eig(M(ep))
    w = np.real(w)
    p = np.matrix(np.real(p))
    
    w_Temp = []
    f = []
    for branch in branches:
        f+=[polyEig(ep,eps0,5,branch,Lm.Result[0])]
    
    # Assign the eigenvalue to the branch with the closest one. NB: this fails because of mode veering. Fix below.
    for i in range(3):
        wIndTemp = np.argsort(abs(w-f[i]))
        w_Temp += [deepcopy(w[wIndTemp[0]])]
        if i == 0:
            p_Temp = deepcopy(p[:,wIndTemp[0]])
        else:
            p_Temp = np.hstack((p_Temp,deepcopy(p[:,wIndTemp[0]])))
        w = np.delete(w,wIndTemp[0])
        p = np.delete(p,wIndTemp[0],axis=1)
   
    # Re-order branches so that the order of exact is the same as the order of perturb (this is empirical, not implemented well)   
    if ep < -0.78251800000000005:
        w2 = w_Temp[2]
        w_Temp[2] = w_Temp[0]
        w_Temp[0] = w2
        p2 = p_Temp[:,2]
        p_Temp[:,2] = p_Temp[:,0]
        p_Temp[:,0] = p2    
        
    w_Exact += [w_Temp]
    p_Exact += [p_Temp]
    
    if ep == 0:
        for nB in range(3):
            branch = branches[inds[nB]]
            p_Exact[n][:,nB] = Lm.Result[1][0][branch]


#%% Create data_structure for plots
pert_eigVals = {}       
pert_eigVecs = {}
eps = eps
exact_eigVals = {}
exact_eigVecs = {}

for nB, branch in enumerate(branches):
    lamp = []
    lame = []
    vp = []
    ve = []
    branch = branches[inds[nB]]
    for k in range(order+1):
         lamp += [Lm.Result[0][branch[0:k]]]
         vp += [Lm.Result[1][k][branch]]   
    for n, ep in enumerate(eps):        
        lame += [w_Exact[n][nB]]
        ve += [p_Exact[n][:,nB]]
    pert_eigVals[branch] = np.array(lamp)
    pert_eigVecs[branch] = vp
    exact_eigVals[branch] = lame
    exact_eigVecs[branch] = ve

#%% Save data
L = Lm
pickle.dump([branches,pert_eigVals,pert_eigVecs,eps,exact_eigVals,exact_eigVecs,L], open("Degenerate_Membrane_ins"+str(inset)+".pickle", "wb" ) )
