#_____________________________________________________________________
# Copyright (c) 2018 Georg Mensah et al.
# All rights reserved.
#
# Last edited by: 
#
# Date: 
#
# This file is part of PyHoltz.
# PyHoltz is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Lesser General Public License as published by 
# the Free Software Foundation, version 3 of the License. 
#
# PyHoltz is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of 
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License 
# (COPYING.LESSER) along with PyHoltz.
# If not, see <https://www.gnu.org/licenses/>.
# ____________________________________________________________________
'''
This is a python port of the Orr-Sommerfeld example from the matlab toolbox "NLEVP: A Collection of Nonlinear Eigenvalue Problems"
by  T. Betcke, N. J. Higham, V. Mehrmann, C. Schröder, and F. Tisseur. 

See
T. Betcke, N. J. Higham, V. Mehrmann, C. Schröder, and F. Tisseur, NLEVP: A Collection of Nonlinear Eigenvalue Problems, MIMS EPrint 2011.116, December 2011.
or
T. Betcke, N. J. Higham, V. Mehrmann, C. Schröder, and F. Tisseur, NLEVP: A Collection of Nonlinear Eigenvalue Problems. Users' Guide, MIMS EPrint 2010.117, December 2011.

The original toolbox is available at :
http://www.maths.manchester.ac.uk/our-research/research-groups/numerical-analysis-and-scientific-computing/numerical-analysis/software/nlevp/
'''
#import os 
#os.chdir('../')

import numpy as np
import numpy.matlib as npm
from PyHoltz.NLEVP.all_algebra import pow1, pow2, pow_a
from PyHoltz.NLEVP.parameter import para_model
from matplotlib import pyplot as plt
import time

def cheb(N):
    # Compute D = differentiation matrix, x = Chebyshev grid.
    # See Lloyd N. Trefethen. Spectral Methods in MATLAB. Society for Industrial
    # and Applied Mathematics, Philadelphia, PA, USA, 2000.
    if N== 0:
        D=0
        x=1
        return D,x
    
    x = np.matrix(np.cos(np.pi*np.r_[0:N+1]/N)).H
    c = np.matrix(np.hstack((2, np.ones(N-1),2))*((-1)**np.r_[0:N+1])).H
    X = npm.repmat(x,1,N+1)     
    dX= X-X.H
    D =(c*(1/c.H))/(dX+np.eye(N+1)) # off-diagonal entries
    D=D -np.diag(np.array(np.sum(D,1))[:,0]) #diagonal entries
    return D,np.array(x)[:,0]

def orr_sommerfeld(N=64, R=5772, w =0.26943):
    # Define the Orr-Sommerfeld operator for spatial stability analysis.
    # N: number of Cheb points, R: Reynolds number, w: angular frequency
    N=N+1
    # 2nd- and 4th-order differntiation matrices:
    D,x=cheb(N); D2 =D**2;  D2 = D2[1:N,1:N]
    S= np.diag(np.hstack((0, 1/(1-x[1:N]**2),0)))
    D4=(np.diag(1-x**2)*D**4-8*np.diag(x)*D**3-12*D**2)*S
    D4 = D4[1:N,1:N]

    I=npm.eye(N-1)          
    # type conversion
    I=I.astype('complex')
    D2=D2.astype('complex') 
    D4=D4.astype('complex')    
     
    # Build Orr-Sommerfeld operator
    L=para_model()
    L.add_param('ω',1.) #eigenvalue is always present
    L.add_param('w',w)
    L.add_param('R',R)
    L.add_param('λ',0)
    L.add_term({'matrix':I,'functions':[pow_a(4)],'parameters':[['ω']],'operator':'I'})
    L.add_term({'matrix':1j*npm.diag(1-x[1:N]**2),'functions':[pow_a(3),pow1],'parameters':[['ω'],['R']],'operator':'A3'})
    L.add_term({'matrix':-2*D2,'functions':[pow2],'parameters':[['ω']],'operator':'-2*D2'})
    L.add_term({'matrix':-1j*I,'functions':[pow2,pow1,pow1],'parameters':[['ω'],['w'],['R']],'operator':'-1j*I'})
    L.add_term({'matrix':-1j*(npm.diag(1-x[1:N]**2)*D2)-2j*I,'functions':[pow1,pow1],'parameters':[['ω'],['R']],'operator':'A1'})
    L.add_term({'matrix':1j*D2,'functions':[pow1,pow1],'parameters':[['w'],['R']],'operator':'1jD2'})
    L.add_term({'matrix':D4,'functions':[],'parameters':[],'operator':'D4'}) 
    L.add_term({'matrix':-I,'functions':[pow1],'parameters':[['λ']],'operator':'-I'})
    return L,x           
    
#%%
# Define Orr_Sommerfeld operator
L,x=orr_sommerfeld(N=64)   
# Calculate an eigensoltion
L.householder(1,order=3,maxiter=50)

#%% Run perturbation theory
order = 30
L.p/=L.p[0].item(0) # Set phase at inlet to be zero for convenience (plotting reasons)
time_p = time.time()
L.perturb_parallel('R',order,normalize=True)
time_p = time.time()-time_p
print('Time for '+str(order)+' order perturbation theory: '+str(time_p)+' seconds')

# Post-process solution format
sol=L.solution()
sol.normalize_pert()
lam=L.omega_pert.copy()

#%% Calculate exact soltuions by solving the nonlinear eigenvalue problems for 100 values of epsilon
expand=1.0
start=sol.eps0-5601
stop=sol.eps0+5601           
N_sol=101            
eps=np.linspace(start,stop,N_sol)
eps=np.round(eps).astype(int)
Eps=np.linspace(start,stop,1001)

lam_exact=[]
v_exact=[]
e=eps[50]

# forward
tempsol=sol
time_e = time.time()
for idx,e in enumerate(eps[50:]):
    if not idx%10:
        print('Calculating solution',idx+1,'of',np.ceil(N_sol/2))
    L.set_value(sol.epsilon,e)
    L.householder(tempsol.poly(e,3),tol=1E-12,maxiter=30,output=False,order=1,printRes=False)
    lam_exact+=[L.omega.item(0)]
    v_exact+=[L.p]
    L.perturb_parallel(sol.epsilon,3,normalize=True,printOrd=False)
    tempsol=L.solution()

# backward
tempsol=sol
for idx,e in enumerate(reversed(eps[0:50])):
    if not idx%10:
        print('Calculating solution',idx+1,'of',np.ceil(N_sol/2))
    L.set_value(sol.epsilon,e)
    L.householder(tempsol.poly(e,3),tol=1E-12,maxiter=30,output=False,order=1,printRes=False)
    lam_exact=[L.omega.item(0)]+lam_exact
    v_exact=[L.p]+v_exact
    L.perturb_parallel(sol.epsilon,3,normalize=True,printOrd=False)
    tempsol=L.solution()    

time_e = time.time()-time_e
print('Time for '+str(N_sol)+' exact solutions: '+str(time_e)+' seconds')    
        
    
# Normalize exact v with phase condition <v_0|v(eps)>=Real
lam_exact=np.array(lam_exact)
v0=sol.p_pert[0]
for i in range(len(v_exact)):
    v_exact[i]/=np.exp(np.angle(v0.H*v_exact[i])*1j).item(0)
    v_exact[i]/=np.linalg.norm(v_exact[i])
x=x[1:-1]

#%% Save data. If the above has already been run, load the data and plot results
#import pickle
#pickle.dump([lam_exact,v_exact,x,sol,order,v0,eps], open("PyHoltz/Examples/OrrSommerfeld_data.pickle", "wb" ) )    

#%% Radius of convergence plot
alestest=np.array(sol.omega_pert)
plt.close('all')
plt.figure('convergence',figsize=(6.4/1.3*2*.75,4.8/1.3))
plt.clf()
plt.plot(np.r_[1:order],np.abs(alestest[1:-1]/alestest[2:]),'.',ms=8)
plt.yticks([5772,6000,7000,8000,9000,10000,11000],[r'$\mathbf{\mathrm{\mathbf{Re}}_0 = 5772}$','', '7000','8000','9000','10000','11000'])
plt.grid('on')
ax=plt.gca()
a=ax.get_ygridlines()
b = a[0]
b.set_color('k')
b.set_linewidth(1.5)
plt.xlabel('order N',fontsize=12)
plt.ylabel(r'$\delta_N:=|\lambda_N/\lambda_{N+1}|$',fontsize=12)    
plt.tight_layout()

#%% 
plt.figure('Residual')
plt.clf()
results=[]
for i,e in enumerate(eps):
    results+=[np.linalg.norm(v_exact[i]-sol.polyP(e,order)) ]
plt.plot(eps,results)

#%% Power series vs exact
cmap=plt.get_cmap('viridis_r')
cmap2=plt.get_cmap('magma_r')
colors=cmap(np.linspace(0.,1.,6))
colors2=cmap2(np.linspace(0.,1.,6))

plt.figure('eigval',figsize=(6.4/1.3*2,4.8/1.3))
plt.clf()
plt.subplot(121)
plt.title('a) eigenvalue evolution')

mode0=sol.v[0]
plt.subplot(222)
plt.plot(x,np.abs(mode0),'--k',label='base')
plt.subplot(224)
plt.plot(x,np.angle(mode0),'--k',label='base')

exmpl=0
levels=[1,2,5,10,15,30]
levels=[1,2,5,10,15,25,50]

for N, clr in zip(levels,cmap(np.linspace(0.,1,len(levels)))):#,25]:#range(0,25):
    lams=sol.poly(Eps,N)
    plt.subplot(121)
    plt.plot(np.real(lams),np.imag(lams),color=clr,label='N='+str(N))
    mode=sol.polyP(eps[exmpl],N)
    plt.subplot(222)
    plt.plot(x,np.abs(mode),color=clr,label='N='+str(N))
    plt.subplot(224)    
    plt.plot(x,np.angle(mode),color=clr,label='N='+str(N))

plt.subplot(121)    
plt.plot(np.real(sol.omega),np.imag(sol.omega),'ko')
plt.plot(np.real(lam_exact),np.imag(lam_exact),'k', label='exact')
plt.arrow(np.real(lam_exact[-2]),np.imag(lam_exact[-2]),np.real(lam_exact[-1]-lam_exact[-2]),np.imag(lam_exact[-1]-lam_exact[-2]),fc="k",ec="k",head_width=.01)
plt.grid('on')
plt.xlabel('real($\lambda$)')
plt.ylabel('imag($\lambda$)')
plt.xticks(np.linspace(0.6,1.25,14),['0.6','','0.7','','0.8','','0.9','','1.0','','1.1','','1.2',''])

mode=v_exact[exmpl]
plt.subplot(222)
plt.plot(x,np.abs(mode),'k',label='exact')
plt.legend(bbox_to_anchor=(-1.24,+1.4,2.24, 0.102), loc=3, mode = "expand", borderaxespad=0,ncol=5)
plt.grid('on')
plt.ylabel('abs$(\mathbf{v} (y))$',labelpad=13)
plt.title(r'b) mode shape Re='+str(eps[exmpl]))
plt.subplot(224)    
plt.plot(x,np.angle(mode),'k',label='exact')
plt.ylim(-0*1.1*np.pi,np.pi*1.1/2)
plt.yticks(np.linspace(0,np.pi/2,3),['$0$','$\pi/4$','$\pi/2$'])
plt.xlabel('$y$')
plt.ylabel('phase$(\mathbf{v} (y))$')
plt.grid('on')

plt.tight_layout(rect=[0,0,1,0.8])
plt.subplot(121)
plt.axis('equal')
f=plt.gcf()
f.subplots_adjust(hspace=0.0)

#%% Convergence of eigsolution 
plt.figure('error',figsize=(12.8/1.3,4.8/1.3))
plt.clf()
L.active=['ω',sol.epsilon]
levels=[49,38,25,10,0]
for O, color in zip(levels,cmap2(np.linspace(0.15,1.,len(levels)))):
    err=[]
    errV=[]
    errP=[]
    norm=[]
    for N in range(0,order+1):
        err+=[lam_exact[O]-sol.poly(eps[O],N)]
        
        Vp=np.matrix(sol.polyP(eps[O],N))
        V1=Vp.copy()
        
        #minimum residual
        Vp/=np.linalg.norm(Vp)
        vec_a=L(sol.poly(eps[O],N),eps[O])*Vp
        vec_b=L(sol.poly(eps[O],N),eps[O])*sol.p_pert[0]
        c=(vec_a.H*vec_b).item(0)/(vec_b.H*vec_b).item(0)
        Vp=Vp-np.conj(c)*sol.p_pert[0]
        Vp/=np.exp(np.angle(v0.H*Vp)*1j).item(0) #stick to phase convention
        Vp/=np.linalg.norm(Vp)
        Vx=v_exact[O].copy()
        Vx/=np.linalg.norm(Vx)        
        res0=Vx-Vp
        
        #iterative scheme
        Vp=np.matrix(sol.polyP(eps[O],N))
        Vx=v_exact[O].copy()
        res=Vx-Vp 
       
        errP+=[np.sqrt(res0.H*res0).item(0)]
        errV+=[np.sqrt(res.H*res).item(0)]
        
    err=np.array(err)
    errV=np.array(errV)
    plt.figure('error')
    plt.subplot(121)    
    plt.semilogy(np.abs(err),color=color,label='Re='+str(eps[O]))
    plt.subplot(122)
    plt.semilogy(np.abs(errP),'-',color=color,label='Re='+str(eps[O]))
    
    #plt.gca().set_prop_cycle(None)
    plt.hold('all')
    plt.semilogy(np.abs(errV),'--',color=color,label='__nolabel__',lw=3)
    plt.hold('on')
    #plt.figure('norm')
    plt.semilogy(norm)

plt.subplot(121)
plt.grid('on')
plt.xlabel('perturbation order $N$')
plt.ylabel('$|\lambda_\mathrm{exct}-\lambda_\mathrm{pert}|$')
plt.title('c) eigenvalue error')
plt.xticks(np.linspace(0,order,11))
plt.legend(bbox_to_anchor=(0,-.35,2.24, 0.102), loc=3, mode = "expand", borderaxespad=0,ncol=5)
plt.subplot(122)
plt.grid('on')
plt.xlabel('perturbation order $N$')
plt.xticks(np.linspace(0,order,11))
plt.ylabel(r'$||\mathbf{v}_\mathrm{exct}-\mathbf{v}_\mathrm{pert}||$')        
plt.title('d) eigenvector error')
L.active=['ω']
plt.tight_layout(rect=[0,0.075,1,1])

