#_____________________________________________________________________
# Copyright (c) 2018 Georg Mensah et al.
# All rights reserved.
#
# Last edited by: 
#
# Date: 
#
# This file is part of PyHoltz.
# PyHoltz is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Lesser General Public License as published by 
# the Free Software Foundation, version 3 of the License. 
#
# PyHoltz is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of 
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License 
# (COPYING.LESSER) along with PyHoltz.
# If not, see <https://www.gnu.org/licenses/>.
# ____________________________________________________________________

"""
Created on Thu Mar 15 14:57:53 2018
@author: orchini
3-fold degenerate eigenvalue problem in structural mechanics, when considering a simply clamped plate's vibrations
The 3-fold degeneracy is due to symmetry (2 modes) + 1 accidental degeneracy.
The accidental degeneracy is removed at first order, the symmetry induced one at second order.
The Chebychev method used to solve the problem is adapted from Trefethen "Spectral Methods" book
"""

import numpy as np
import numpy.matlib as npm
from matplotlib import pyplot as plt
from PyHoltz.NLEVP.all_algebra import pow1
from PyHoltz.NLEVP.parameter import para_model
import time

def cheb(N):
# First order spectral differentiation matrix as defined in Trefethen book
# Domain is [-1:1]    
    if N==0:
        D=0 
        x=1
    else:
        x = np.asarray([np.cos(np.pi*(i)/N) for i in range(N+1)])
              
        c = np.multiply(np.asarray([2] + np.ones(N-1).tolist() + [2]),
                      np.asarray([(-1)**i for i in range(N+1)]))
        X = npm.repmat(x.reshape(N+1,1),1,N+1)
        dX = X-X.transpose()
        D  = ( np.outer(c,1/c) )/(dX+(np.eye(N+1)))      #% off-diagonal entries
        D  = D - np.diag(np.sum(D,1))               #% diagonal entries     
    return D, x

#%%

isPlot = False

N = 12+1
D, xx = cheb(N)
scaleX = 2
scaleY = (1+np.sqrt(5))
x = xx/scaleX
y = xx/scaleY
# The chebychev matrices are space dependent! scale them accordingly.
Dx = D*scaleX
Dy = D*scaleY

D2x = Dx@Dx
D2y = Dy@Dy

Dx =  Dx[1:N,1:N]
D2x = D2x[1:N,1:N]
Dy =  Dy[1:N,1:N]
D2y = D2y[1:N,1:N]

# Apply BC
I = np.eye(N-1)
L = np.kron(I,D2x) + np.kron(D2y,I)
               
[xxg,yyg] = np.meshgrid(x[1:N],y[1:N])
xxL = xxg.reshape((N-1)**2,1)
yyL = yyg.reshape((N-1)**2,1)

#%% Define space-dependent perturbation
pert1 = (np.cos(np.pi*2*xxL)*np.cos(np.pi*yyL)).flatten()
nx, ny = 1,1
eigVal = np.pi**4*((nx*scaleX/2)**2 + (nx*scaleY/2)**2)**2                                  
M1 = np.diag(pert1)
L0 = L@L + 0j
L1 = 1000*M1 + 0j

#%% Define the operator
Lm=para_model()

Lm.add_param('ω',1.)
Lm.add_param('eps',0.0)
Lm.add_param('λ',0)

Lm.add_term({'matrix':np.matrix(L0),'functions':[],'parameters':[],'operator':'L0'})
Lm.add_term({'matrix':np.matrix(-np.eye((N-1)**2)),'functions':[pow1],'parameters':[['ω']],'operator':'-I'})
Lm.add_term({'matrix':np.matrix(L1),'functions':[pow1],'parameters':[['eps']],'operator':'L1'})
Lm.add_term({'matrix':np.matrix(-np.eye((N-1)**2)),'functions':[pow1],'parameters':[['λ']],'operator':'-λ'})

#%% Calcualte solution and compare with known theoreticalr result
m,n=1,1
expected_eigenvalue=np.pi**4*((m)**2/(2/scaleX)**2+(n)**2/(2/scaleY)**2)**2
Lm.householder(1200,maxiter=10,tol=1E-7,n_eig_val=1)
print('Expected eigenvalue:', expected_eigenvalue)
print('Obtained eigenvalue:', Lm.omega)
print('Absolute error:', np.abs(Lm.omega-expected_eigenvalue))

#%% Run perturbation theory
order = 30
time_p = time.time()
Lm.perturb_parallel('eps',order,normalize=True)
NN=N-1
time_p = time.time()-time_p
print('Time for '+str(order)+' order perturbation theory: '+str(time_p)+' seconds')

#%% Calculate exact solutions
L=Lm
sol=L.solution()   
sol.estimate_pol()
lam=sol.omega_pert.copy()
start=-14.5
stop = +14.5
N_sol = 101
eps=np.linspace(start,stop,N_sol)
Eps=np.linspace(start,stop,1001)

lam_exact=[]
v_exact=[]
e=eps[50]
tempsol=sol

time_e = time.time()
for idx,e in enumerate(eps[50:]):
    if not idx%10:
        print('Calculating solution',idx+1,'of',np.ceil(N_sol/2))
    L.set_value(sol.epsilon,e)
    L.householder(tempsol.poly(e,3),tol=1E-7,maxiter=30,output=False,order=1,printRes=False)
    lam_exact+=[L.omega.item(0)]
    v_exact+=[L.p]
    Lm.perturb_parallel(sol.epsilon,3,normalize=True,printOrd=False)
    tempsol=L.solution()

tempsol=sol
for idx,e in enumerate(reversed(eps[0:50])):
    if not idx%10:
        print('Calculating solution',idx+1,'of',np.ceil(N_sol/2))
    L.set_value(sol.epsilon,e)
    L.householder(tempsol.poly(e,3),tol=1E-7,maxiter=30,output=False,order=1,printRes=False)
    lam_exact=[L.omega.item(0)]+lam_exact
    v_exact=[L.p]+v_exact
    Lm.perturb_parallel(sol.epsilon,3,normalize=True,printOrd=False)
    tempsol=L.solution()    

time_e = time.time()-time_e
print('Time for '+str(N_sol)+' exact solutions: '+str(time_e)+' seconds')    
    
lam_exact=np.array(lam_exact)

#%% Save some data
#import pickle
#pickle.dump([lam_exact,v_exact,sol,order], open("../RectangularMembrane_data.pickle", "wb" ) )  

#%% Normalize exact v
v0=sol.p_pert[0]
for i in range(len(v_exact)):
    v_exact[i]/=np.exp(np.angle(v0.H*v_exact[i])*1j).item(0)
    v_exact[i]/=np.linalg.norm(v_exact[i])

#%% Plot
plt.close('all')
plt.figure('3')
plt.clf()
results=[]
for i,e in enumerate(eps):
    results+=[np.linalg.norm(v_exact[i]-sol.polyP(e,order)) ]
plt.plot(eps,results)
cmap=plt.get_cmap('viridis_r')
cmap2=plt.get_cmap('magma_r')

#%%
plt.figure('eigval',figsize=(6.4/1.3,4.8/1.3))
plt.clf()
plt.title('a) eigenvalue evolution')
plt.figure('modeshape',figsize=(6.4/1.3,4.8/1.3))
plt.clf()
plt.subplot(3,3,1)
plt.contourf(xxL.reshape(NN,NN),yyL.reshape(NN,NN),np.abs(sol.p_pert[0]).reshape(NN,NN))
plt.xticks([]), plt.yticks([])
plt.title('base')
plt.axis('equal')
plt.axis('tight')
MAX=np.abs(sol.p_pert[0]).max()
plt.clim(0,MAX)
exmpl=87

levels=[1,2,5,10,15,20,30]
for idx,(N, clr) in enumerate(zip(levels,cmap(np.linspace(0.,1.,len(levels))))):#,15,30]:#,25]:#range(0,25):
    lams=sol.poly(Eps,N)
    plt.figure('eigval')    
    plt.plot(Eps,np.real(lams),label='N='+str(N),color=clr)
    plt.figure('modeshape')
    plt.subplot(3,3,idx+2)
    plt.contourf(xxL.reshape(NN,NN),yyL.reshape(NN,NN),np.abs(sol.polyP(eps[exmpl],N)).reshape(NN,NN))
    plt.xticks([]), plt.yticks([])
    plt.title('N='+str(N))
    plt.clim(0,MAX)


plt.figure('eigval')        
plt.plot(eps[50],np.real(sol.omega),'ko')
plt.plot(eps,np.real(lam_exact),'k')
plt.legend()
plt.grid('on')
plt.xlabel('$\epsilon/10^{3}$')
plt.ylabel('$\lambda$')
plt.ylim(-10000,+10000)
plt.tight_layout()
plt.figure('modeshape')
plt.subplot(3,3,9)
plt.contourf(xxL.reshape(NN,NN),yyL.reshape(NN,NN),np.abs(v_exact[exmpl]).reshape(NN,NN))
plt.xticks([]), plt.yticks([])
plt.title('exact')
plt.clim(0,MAX)
plt.suptitle( r'b) modeshape $\epsilon='+str(np.round(eps[exmpl],4) )+r'\times 10^3$')
plt.tight_layout(rect=[0,0,1,.95])

#%%
plt.figure('error',figsize=(12.8/1.3,4.8/1.3))
plt.clf()
L.active=['ω',sol.epsilon]
levels=[51,62,75,90,100]
for O, color in zip(levels,cmap2(np.linspace(0.15,1.,len(levels)))):
    err=[]
    errV=[]
    errP=[]
    norm=[]
    for N in range(0,order+1):
        err+=[lam_exact[O]-sol.poly(eps[O],N)]     
        
        Vp=np.matrix(sol.polyP(eps[O],N))
        V1=Vp.copy()
        
        #minimum residual
        Vp/=np.linalg.norm(Vp)
        vec_a=L(sol.poly(eps[O],N),eps[O])*Vp
        vec_b=L(sol.poly(eps[O],N),eps[O])*sol.p_pert[0]
        c=(vec_a.H*vec_b).item(0)/(vec_b.H*vec_b).item(0)
        Vp=Vp-np.conj(c)*sol.p_pert[0]
        Vp/=np.exp(np.angle(v0.H*Vp)*1j).item(0) #stick to phase convention
        Vp/=np.linalg.norm(Vp)
        Vx=v_exact[O].copy()
        Vx/=np.linalg.norm(Vx)          
        res0=Vx-Vp
        
        #iterative scheme
        Vp=np.matrix(sol.polyP(eps[O],N))
        Vx=v_exact[O].copy()
        res=Vx-Vp 
       
        errP+=[np.sqrt(res0.H*res0).item(0)]
        errV+=[np.sqrt(res.H*res).item(0)]
        
    err=np.array(err)
    errV=np.array(errV)
    plt.subplot(121)    
    plt.semilogy(np.abs(err),color=color,label=r'$\varepsilon$='+str(np.round(eps[O],4))+r'$\times 10^3$')
    plt.subplot(122)
    plt.semilogy(np.abs(errP),'-',color=color,label=r'$\varepsilon$='+str(np.round(eps[O],4))+r'$\times 10^3$')
    
    plt.hold('all')
    plt.semilogy(np.abs(errV),'--',color=color,label='__nolabel__')
    plt.hold('on')
    plt.semilogy(norm)

plt.subplot(121)
plt.grid('on')
plt.xlabel('perturbation order $N$')
plt.ylabel('$|\lambda_\mathrm{exct}-\lambda_\mathrm{pert}|$')
plt.title('c) eigenvalue error')
plt.xticks(np.linspace(0,30,7))
plt.legend(bbox_to_anchor=(0,-.35,2.24, 0.102), loc=3, mode = "expand", borderaxespad=0,ncol=5)
plt.subplot(122)
plt.grid('on')
plt.xlabel('perturbation order $N$')
plt.xticks(np.linspace(0,30,7))
plt.ylabel('$||\mathbf{v}_\mathrm{exct}-\mathbf{v}_\mathrm{pert}||$')        
plt.title('d) eigenvector error')
L.active=['ω']
plt.tight_layout(rect=[0,0.075,1,1])
