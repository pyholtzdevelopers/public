#_____________________________________________________________________
# Copyright (c) 2018 Georg Mensah et al.
# All rights reserved.
#
# Last edited by: 
#
# Date: 
#
# This file is part of PyHoltz.
# PyHoltz is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Lesser General Public License as published by 
# the Free Software Foundation, version 3 of the License. 
#
# PyHoltz is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of 
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License 
# (COPYING.LESSER) along with PyHoltz.
# If not, see <https://www.gnu.org/licenses/>.
# ____________________________________________________________________
"""
Created on Fri Feb 23 14:21:43 2018
@author: orchini
Plotting routine for Hydrogen atom
"""

import os
os.chdir('../')


from matplotlib import pyplot as plt
import numpy as np

def plot_degenerate_perturb(Result,branch_num,exact,order,fig_name):
    
    plt.close('all')
    plt.figure(1,figsize=(20/2.54, 12/2.54))
    branches = [i for i in Result[0].keys() if len(i)==order]
    branch = branches[branch_num]
    omega_old = 0j
    N = len(Result[1][0][branch])
    hand = []
    
    eps = exact['eps']
    plt.subplot(121)
    plt.plot([0, 0.4], [np.finfo(float).eps,np.finfo(float).eps],'k--')
    plt.subplot(122)
    plt.plot([0, 0.4], [N*np.finfo(float).eps,N*np.finfo(float).eps],'k--')
    
    om_store = np.matrix(np.zeros(shape=(len(range(0,order+1,2)), len(eps))))
    p_store = np.matrix(np.zeros(shape=(len(range(0,order,2)), len(eps))))
    
    #%%
    for n, ep in enumerate(eps):
     
        omega = np.zeros((order+1,1),dtype=complex)
        pvec = np.matrix(np.zeros((N,order),dtype=complex))        
        w = exact['omega'][n]
        p = exact['p'][n]
       
        for orders in range(order+1):
            omega[orders] += Result[0][branch[0:0]]
            if orders != order:
                pvec[:,orders] += Result[1][0][branch]    
            for i in range(1,orders):
                omega[orders] += Result[0][branch[0:i]]*ep**i
                if orders != order:
                    pvec[:,orders] += Result[1][i][branch]*ep**i    
    
        if n < 20:
            ind = np.argmin(np.abs(w-omega[order-1]))
        else:
            ind = np.argmin(np.abs(w-omega_old))
        
        w_r = w[ind]
        p_r = p[:,ind]
        omega_old = w_r                      
    
    
        for ind_ord, orders in enumerate(range(0,order+1,2)):
            if n == 0:
                plt.subplot(121)
                hand+=plt.semilogy(ep,np.abs(w_r-omega[orders]+np.finfo(float).eps)/(max(np.abs(w_r),1.)),'.',color=np.array([1, 1, 1])*(orders)/order,
                                label='$\epsilon^{'+str(orders)+'}$')
            else:
                plt.subplot(121)
                plt.semilogy(ep,np.abs(w_r-omega[orders]+np.finfo(float).eps)/(max(np.abs(w_r),1.)),'.',color=np.array([1, 1, 1])*(orders)/order)        
            om_store[ind_ord,n] = np.abs(w_r-omega[orders]+np.finfo(float).eps)/(max(np.abs(w_r),1.))
            
            if orders != order:
                plt.subplot(122)
                p_store[ind_ord,n] = min(np.linalg.norm(p_r-pvec[:,orders].T)+np.finfo(float).eps,
                            np.linalg.norm(p_r+pvec[:,orders].T)+np.finfo(float).eps)
                plt.semilogy(ep,p_store[ind_ord,n],'.',color=np.array([1, 1, 1])*(orders)/order)
            
            
    #%%    
    plt.subplot(121)
    bool_eps = np.squeeze(np.asarray((om_store[-1,:]-om_store[-2,:] > 0) & (om_store[-1,:] > 1e-6) ))
    if sum(bool_eps) == 0:
        bool_eps[-1] = True
    om_ind = np.array(range(len(eps)))
    om_ind = om_ind[bool_eps]
    
    plt.tick_params(axis='both', which='major', labelsize=11)
    plt.tick_params(axis='both', which='minor', labelsize=8)
    plt.xlabel(r"$\epsilon$",fontsize=14)
    plt.ylabel(r"$|\lambda - \lambda_n|$",fontsize=14)
    plt.grid(color=[0.25, 0.25, 0.25], linestyle='-', linewidth=0.1)
    plt.annotate('machine precision', xy=(0.05, np.finfo(float).eps), xytext=(0.08, 1.5e-12),
                arrowprops=dict(arrowstyle="->"))
    
    plt.plot(eps[om_ind[0]],om_store[-1,om_ind[0]],'sr',markerfacecolor='none',mew=1.5,markersize=8)
    plt.plot([eps[om_ind[0]],eps[om_ind[0]]],[np.finfo(float).eps/100,om_store[-1,om_ind[0]]],'--r',linewidth=0.7)
    
#    plt.xlim([0, 0.4])
#    plt.ylim([1e-16, 10])
    
    
    plt.subplot(122)
    bool_p = np.squeeze(np.asarray((p_store[-1,:]-p_store[-2,:] > 0) & (p_store[-1,:] > 1e-6 ) ))
    if sum(bool_p) == 0:
        bool_p[-1] = True
    p_ind = np.array(range(len(eps)))
    p_ind = p_ind[bool_p]
    
    plt.tick_params(axis='both', which='major', labelsize=11)
    plt.tick_params(axis='both', which='minor', labelsize=8)
    plt.xlabel(r"$\epsilon$",fontsize=14)
    plt.ylabel(r"$|\!|\mathbf{v} - \mathbf{v}_n|\!|$",fontsize=14)
    plt.grid(color=[0.25, 0.25, 0.25], linestyle='-', linewidth=0.1)
    #plt.annotate(r'N $\times$ machine' + '\n   precision', xy=(0.05, N*np.finfo(float).eps), xytext=(0.08, 0.9e-12),
    #            arrowprops=dict(arrowstyle="->"))
    plt.annotate('Convergence \n    radius', xy=(eps[p_ind[0]], 1e-16), xytext=(0.08, 0.9e-12),
                 arrowprops=dict(arrowstyle="->",color='r'),color='r')
    plt.plot(eps[p_ind[0]],p_store[-1,p_ind[0]],'sr',markerfacecolor='none',mew=1.5,markersize=8)
    plt.plot([eps[p_ind[0]],eps[p_ind[0]]],[np.finfo(float).eps/100,p_store[-1,p_ind[0]]],'--r',linewidth=0.7)
#    
#    plt.xlim([0, 0.4])
#    plt.ylim([1e-16, 10])
    
    plt.legend(handles=hand,bbox_to_anchor=(-1.41, 1.02, 1., .102), loc=3,ncol=len(hand))
    
    plt.tight_layout(rect=[0, 0, 1, 0.925])
    
    
    plt.show()
    
#    plt.savefig(fig_name, format='eps', bbox_inches='tight', dpi=1000)
