#_____________________________________________________________________
# Copyright (c) 2018 Georg Mensah et al.
# All rights reserved.
#
# Last edited by: 
#
# Date: 
#
# This file is part of PyHoltz.
# PyHoltz is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Lesser General Public License as published by 
# the Free Software Foundation, version 3 of the License. 
#
# PyHoltz is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of 
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License 
# (COPYING.LESSER) along with PyHoltz.
# If not, see <https://www.gnu.org/licenses/>.
# ____________________________________________________________________
"""
This routine calculates the eigenvalues and eigenvectors of a thermoacoustic system. The geometry resembles that of the MICCA combustor, developed at EM2C, Paris.
A mesh is loaded and the Helmholtz equation with heat release (modelled by an FTF) is solved on it. Because of the rotational symmetry of the system, some eigenvalue are 2-fold degenerate
We compute a degenerate baseline solution using a Newton solver routine (with knowledge on the level of degeneracy).
This solution is fed to the degenerate perturbation algrotith, which calculates the power series approximation of eigenvalues and eigencvectors up to the desired order.  
"""
#import os
#os.chdir('/home/orchini/PyHoltzDevel/devel/')

from importlib import reload
from PyHoltz.FEM.mesh_class import mesh 
from  PyHoltz.FEM.discretization import discretization
import numpy as np
from PyHoltz.NLEVP.all_algebra import pow0,pow1,pow2,ntau2, ntau2
from scipy import interpolate
from PyHoltz.FEM.vtk_write import vtk_write
from PyHoltz.FEM.mesh_utils import smplx_sorter, smplx_finder, cart2pol, pol2cart, gen_index 
from time import process_time,time

from scipy.interpolate import interp1d
from copy import deepcopy

Bloch='extent'
test_case='Georg'
msh=mesh('./PyHoltz/mesh/micca_coarse.msh')

for node in msh.Domains['Flame']['nodes']:
    msh.Domains['Interior']['nodes']=smplx_sorter([node],msh.Domains['Interior']['nodes'])

print(msh)

def polyEig(eps,eps0,order,branch,lamps):
    lam = 0.
    for k in range(order+1):
        lam += (eps - eps0)**k*lamps[branch[0:k]]
    return lam

def polyVec(eps,eps0,order,branch,vecps):
    vec = vecps[0][branch]
    for k in range(1,order+1):
        vec += (eps - eps0)**k*vecps[k][branch]
    return vec

#%% From 1/2 cell to cell
msh.mirror()
print(msh)
msh.compute_domain_volume('Flame')

#%%
ToBeFused=['Outlet','Interior','Injection','Flame']
msh.merge_with_img(ToBeFused)

#%%    
ToBeKept=['Outlet','Interior','Bloch','Bloch_img','Injection','Flame']
msh.keep(ToBeKept)
        
#%% Define model parameters
M=28.8 # Molar mass air (in g)
R=8314 # Gas constant (in g)
gamma=1.4 # Heat specific raio
rho=1.1878 # Density
ref_vec=np.array([0,0,1])
n=1 # Interaction index
tau=3E-3 # Time delay
FTF=ntau2(n,tau)                      
lcc=0.2
z_in=0 # inlet axial location
T0=300 # Inlet temperature

def FTFwrapper(omega,n,tau,d_omega,d_n,d_tau):
    return FTF([n,tau],[d_n,d_tau],omega,d_omega)

#%% Temperature distribution, thanks to Davide Laera
T=np.loadtxt('./PyHoltz/Examples/MICCA_T_Standing.txt')

T[:,0]-=20
temperature=interp1d(T[:,0],T[:,1],kind=3,fill_value=(300,1200),bounds_error=False)
x=np.linspace(T[0,0],T[-1,0],10000)
y=temperature(x)

#%%
if test_case=='Georg':
    def FTF(w,n):
        return ftf(w,n)

    x_ref=np.array([-.175, 0, -0.006-0.014-0.0001])
    u_mean=0.66#[m/s] 
    q_mean=2080#[W] 
    Q02u0=q_mean/u_mean
    def temp(x,y,z):
        return temperature(z*1000)

elif test_case=='Davide':
    def FTF(w,n):
        return ftf(np.real(w),n)

    x_ref=np.array([-.175, 0, -0.006-0.014-0.0001])
    u_mean=0.66#[m/s]
    q_mean=2080#[W]
    Q02u0=q_mean/u_mean
    def temp(x,y,z):
        return temperature(z*1000)

elif test_case=='Ale':
    def FTF(w,n):
        return ftf(np.real(w),n)
    T1=1400
    T2=1400
    T3=1400
    x_ref=np.array([-.175, 0, -0.00-0.0001])
    P_0 =101325 #rho*R/M*T0# 
    A=(0.025/2)**2*np.pi
    Q02u0 = P_0*(T1/T0-1)*A*gamma/(gamma-1)
    fill_value=(T1,T3)

    y=[T1,T2,T3]
    x=[z_in+0.02,z_in+0.02+lcc/2,z_in+0.02+lcc]
    f = interpolate.interp1d(x, y,kind=2,bounds_error=False,fill_value=fill_value)
            
    def temp(x,y,z):
        if (z<z_in):
            return T0
        else:
            return f(z) 

cfunc = lambda x, y, z: np.sqrt(gamma*R/M*temp(x,y,z))
c_field=np.empty((len(msh.Points),1))
for i, pnt in enumerate(msh.Points):
    c_field[i]=cfunc(pnt[0],pnt[1],pnt[2]) #TODO: vectorize   

for idx in msh.Domains['Injection']['nodes']:
    c_field[idx]=np.sqrt(gamma*R/M*T0)
    
#%% Define model components
model={ 'Interior':['interior',(),[]],
        'Outlet':['impedance',1e-15,['Y']],
        'Flame':['flame',(rho,gamma,Q02u0,x_ref,ref_vec,FTFwrapper,n,tau),['n','tau']], #TODO: rewrite ntau such that it is seperated as follows f(n)*g(omega,tau)
        }

ToBeKept=list(model.keys())  
if Bloch=='Bloch' or Bloch== 'extent':
    ToBeKept+=['Bloch','Bloch_img']      
msh.keep(ToBeKept)
print(msh)

#%% Build finite difference matrices
discr=discretization(msh,model,c_field,Bloch=Bloch)
L=discr.discretize_operators()
    
#%% The model is now composed by 16 burners. We want to perturb 4 of them, and leave the others unchanged
para='tau'
unify=[0,2,5,9] # Burners to be perturbed
unify=[para+'#'+str(i) for i in unify]
for term in L.terms:
    for param in term['parameters']:
        if len(param)>=2 : print(param[1])
        if len(param)>=2 and param[1] in unify:
            param[1]=para
L.add_param('tau',tau)   

#%% Tidy up after merging matrices
for opr in ['Q','M','C','K']:
    idces=[]
    for idx,term in enumerate(L.terms):   
        if term['operator']==opr:
            if len(term['parameters'])>1 and len(term['parameters'][1])>1 and term['parameters'][1][1] =='tau':
                print(term)
                continue
            print(idx)
            idces+=[idx]
    
    merged_term=L.terms[idces[0]]
    old_terms=[]
    for idx,term in enumerate(L.terms):
        if idx == idces[0]:
            pass
        elif idx in idces[1:]:
            merged_term['matrix']+=term['matrix']
        else:
            old_terms+=[term]
        L.terms=[merged_term]+old_terms

# List variables and their default values.
L.add_term({'matrix':-L.terms[2]['matrix'],'functions':[pow1],'parameters':[['λ']],'operator':'-I'})
L.params=['ω','Y#0','n#0','n#1','n#2','n#5','n#9','tau','tau#1','λ']
L.values=[1,999999999999999.9,1.,1.,1.,1.,1.,tau,tau,0.]

#%% Find a solution. We focus on an azimuthal solution with multiplicity 2
t=time()
L.newt((513.)*np.pi*2,relax=1,maxiter=20,tol=1E-12,n_eig_val=2)
t=time()-t

#%% Run perturbation theory varying the time delay of the perturbed burners 
order = 10
L.perturb_degenerate('tau', N=order, issparse=True, debug=True, rtol=100, normalisation=True)

#%% Save some results, can be seen in paraview       
n_bloch=np.min(msh.Domains['Bloch_img']['nodes'])
def bloch_extent(p,b,N):
    n=p.shape[0]
    m=p.shape[1]
    dphi=np.pi*2/N
    p_ext=np.empty(shape=(n*N,m),dtype='complex')
    for k in range(N):
        p_ext[n*k:n*(k+1)]=p*np.exp(-b*dphi*k*1j)
    return p_ext
p=L.p

cfield=bloch_extent(c_field[:n_bloch],0,16)    
msh.rotate()
vtk_write(msh.Name[0:-4],msh,{'I':np.real(p[:,0])/np.max(np.real(p[:,0])), 
          'II':np.real(p[:,1])/np.max(np.real(p[:,1])),'cfield':np.real(cfield)})#'cfield':np.real(cfield)})#,'cfield':cfield})    

#%% Calculate some exact solutions
omega0 = deepcopy(L.omega)
tau0 = deepcopy(L.get_value('tau'))
branches = [ i for i in L.Result[0].keys() if len(i)==order]
conv_rad = 0.00051 # how do estimate this coming soon
taus = np.linspace(tau0-1.1*conv_rad,tau0+1.1*conv_rad,21)

L1 = deepcopy(L)
w_Exact = []
p_Exact = []

for n, tau in enumerate(taus):
    L1.set_value('tau',tau) 
    w_guess = deepcopy(omega0)

    w_Temp = []
    p_Temp = []
    
    for nB, branch in enumerate(branches):
        w_guess = deepcopy(omega0)
        for ind_Ord in range(1,order+1):
            w_guess += (tau - tau0)**ind_Ord*L.Result[0][branch[0:ind_Ord]]           
        L1.newt(w_guess,relax=1,maxiter=20,tol=1E-12,n_eig_val=1)      
 
        w_Temp += [L1.omega]
        if nB == 0:
            p_Temp = deepcopy(L1.p)
        else:
            p_Temp = np.hstack((p_Temp,L1.p))
        
    w_Exact += [w_Temp]
    p_Exact += [p_Temp]
    
    if tau == tau0:
        for nB in range(2):
            branch = branches[nB]
            p_Exact[n][:,nB] = L.Result[1][0][branch]

#%% Calculate some more exact solutions (finer grid)
taus2 = np.linspace(tau0,tau0+1.1*conv_rad,51)
L2 = deepcopy(L)

w2_Exact = []
p2_Exact = []

for n, tau in enumerate(taus2):
    L2.set_value('tau',tau) 
    w_guess = deepcopy(omega0)

    w_Temp = []
    p_Temp = []
    
    for nB, branch in enumerate(branches):
        w_guess = deepcopy(omega0)
        for ind_Ord in range(1,order+1):
            w_guess += (tau - tau0)**ind_Ord*L.Result[0][branch[0:ind_Ord]]           
        L2.newt(w_guess,relax=1,maxiter=20,tol=1E-12,n_eig_val=1)      
 
        w_Temp += [L2.omega]
        if nB == 0:
            p_Temp = deepcopy(L2.p)
        else:
            p_Temp = np.hstack((p_Temp,L2.p))
        
    w2_Exact += [w_Temp]
    p2_Exact += [p_Temp]
    
    if tau == tau0:
        for nB in range(2):
            branch = branches[nB]
            p2_Exact[n][:,nB] = L.Result[1][0][branch]
          
#%% Create data_structure for plots
pert_eigVals = {}       
pert_eigVecs = {}
taus = taus
exact_eigVals = {}
exact_eigVecs = {}
taus2 = taus2
exact_eigVals2 = {}
exact_eigVecs2 = {}

for nB, branch in enumerate(branches):
    lamp = []
    lame = []
    vp = []
    ve = []
    lame2 = []
    ve2 = []
    for k in range(order+1):
         lamp += [L.Result[0][branch[0:k]]]
         vp += [L.Result[1][k][branch]]   
    for n, tau in enumerate(taus):        
        lame += [w_Exact[n][nB]]
        ve += [p_Exact[n][:,nB]]
    for n, tau in enumerate(taus2):        
        lame2 += [w2_Exact[n][nB]]
        ve2 += [p2_Exact[n][:,nB]]
    pert_eigVals[branch] = np.array(lamp)
    pert_eigVecs[branch] = vp
    exact_eigVals[branch] = lame
    exact_eigVecs[branch] = ve
    exact_eigVals2[branch] = lame2
    exact_eigVecs2[branch] = ve2
    
#%% Save data
import pickle
pickle.dump([branches,pert_eigVals,pert_eigVecs,taus,exact_eigVals,exact_eigVecs,msh,taus2,exact_eigVals2,exact_eigVecs2], open("PyHoltz/Examples/Micca_dat.pickle", "wb" ) )
