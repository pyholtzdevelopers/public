// Script to generate a 3D-Mesh for a Simplified Eveque Chamber (a full slice)
// Philip Buschmann, Georg Mensah, Jonas Moeck, TU Berlin 2015-2016

/////////////////////////////////////////////////////////////
// 		Parameters
// define characteristic length (for mesh size)
lc =  0.1*40;   // general mesh
lcf = 1.0*lc; // flame region

// rotational symmetry
N   = 8;
angle = (2*Pi)/(N); 
phi = angle/2;


// geometric parameters of burner
R  = 0.16;	// distance to rotational axis
dR = 0.12; 	// radial thickness of segment
l1 = 0.2;	// height plenum
l2 = 0.3;	// height CC

ho = (R+dR)*Sin(phi);  // height outer point (in y)
hi = (R)*Sin(phi);     // height inner point (in y)
lo = -(R+dR)*Cos(phi); // length outer point (in x)
li = -(R)*Cos(phi);    // length inner point (in x)

// flame properties
lf = 0.1; // flame thickness (around KOS)

/////////////////////////////////////////////////////////////
//		Points		
// Plenum Bottom
Point(1)  = {-R,0,-(l1-lf/2),lc};
Point(2)  = {-(R+dR),0,-(l1-lf/2),lc};
Point(3)  = {lo,ho,-(l1-lf/2),lc};
Point(4)  = {li,hi,-(l1-lf/2),lc};

// Flame: Towards Bottom
Point(5)  = {-R,0,-lf/2,lcf};
Point(6)  = {-(R+dR),0,-lf/2,lcf};
Point(7)  = {lo,ho,-lf/2,lcf};
Point(8)  = {li,hi,-lf/2,lcf};

// Flame: Towards Top
Point(9)  = {-R,0,lf/2,lcf};
Point(10)  = {-(R+dR),0,lf/2,lcf};
Point(11)  = {lo,ho,lf/2,lcf};
Point(12)  = {li,hi,lf/2,lcf};

// CC Top
Point(13)  = {-R,0,l2+lf/2,lc};
Point(14)  = {-(R+dR),0,l2+lf/2,lc};
Point(15)  = {lo,ho,l2+lf/2,lc};
Point(16)  = {li,hi,l2+lf/2,lc};

// Additional (KOS-origin at every level)
Point(17) = {0,0,-(l1-lf/2),lc};
Point(18) = {0,0,-lf/2,lc};
Point(19) = {0,0,+lf/2,lc};
Point(20) = {0,0,l2+lf/2,lc};


///// LINES
//		Lines (horizontal)
// Plenum Bottom
Line(1)   = {1,2};
Circle(2) = {2,17,3}; // Circle Arc, starting at 2, ending at 3, center at 23 
Line(3)   = {3,4};
Circle(4) = {4,17,1};

// Flame: Bottom
Line(5)   = {5,6};
Circle(6) = {6,18,7};
Line(7)   = {7,8};
Circle(8) = {8,18,5};

// Flame: Top
Line(9)   = {9,10};
Circle(10) = {10,19,11}; 
Line(11)   = {11,12};
Circle(12) = {12,19,9};

// CC Top
Line(13)   = {13,14};
Circle(14) = {14,20,15}; 
Line(15)   = {15,16};
Circle(16) = {16,20,13};

//		Lines (vertical)
// Plenum Bottom to Plenum Top
Line(17)   = {1,5};
Line(18)   = {2,6};
Line(19)   = {3,7};
Line(20)   = {4,8};
// Flame Bottom to Top
Line(21)   = {5,9};
Line(22)   = {6,10};
Line(23)   = {7,11};
Line(24)   = {8,12};
// CC Bottom to CC Top
Line(25)   = {9,13};
Line(26)   = {10,14};
Line(27)   = {11,15};
Line(28)   = {12,16};

// 		Line Loops 
// Plenum
Line Loop(29) = {1,2,3,4};      // Bottom
Line Loop(30) = {17,-8,-20,4};	// Inner
Line Loop(31) = {18,6,-19,-2};	// Outer
Line Loop(32) = {17,5,-18,-1}; 	// phi+
Line Loop(33) = {20,-7,-19,3}; 	// phi-

// Flame
Line Loop(34) = {5,6,7,8};       // Bottom
Line Loop(35) = {21,-12,-24,8};	 // Inner
Line Loop(36) = {22,10,-23,-6};	 // Outer
Line Loop(37) = {21,9,-22,-5}; 	 // phi+
Line Loop(38) = {24,-11,-23,7};  // phi-

// CC
Line Loop(39) = {9,10,11,12};      // Bottom
Line Loop(40) = {13,14,15,16};	   // Top
Line Loop(41) = {25,-16,-28,12};   // Inner
Line Loop(42) = {26,14,-27,-10};   // Outer
Line Loop(43) = {25,13,-26,-9};    // phi+
Line Loop(44) = {28,-15,-27,11};   // phi-


/////////////////////////////////////////////////////////////
//		SURFACES 
// 		Ruled Surfaces
Ruled Surface(45) = {30}; // Plenum Inner
Ruled Surface(46) = {31}; // Plenum Outer
Ruled Surface(47) = {35}; // Flame Inner
Ruled Surface(48) = {36}; // Flame Outer
Ruled Surface(49) = {41}; // CC Inner
Ruled Surface(50) = {42}; // CC Outer

// 		Plane Surfaces
// Plenum 
Plane Surface(51) = {29}; // Bottom
Plane Surface(52) = {32}; // phi+
Plane Surface(53) = {33}; // phi-
// Flame
Plane Surface(54) = {34}; // Bottom
Plane Surface(55) = {37}; // phi+
Plane Surface(56) = {38}; // phi-
// CC
Plane Surface(57) = {39}; // Bottom
Plane Surface(58) = {40}; // Top
Plane Surface(59) = {43}; // phi+
Plane Surface(60) = {44}; // phi-

///// LABEL
Physical Surface("Inlet")        = {51}; 
Physical Surface("Outlet")       = {58};
Physical Surface("FlameIn")      = {54}; 
Physical Surface("FlameOut")     = {57};  
Physical Surface("Symmetry")     = {52,55,59}; 
Physical Surface("Bloch")        = {53,56,60}; 
Physical Surface("Outer_Pre")    = {46}; 
Physical Surface("Inner_Pre")    = {45}; 
Physical Surface("Outer_Flame")  = {48}; 
Physical Surface("Inner_Flame")  = {47}; 
Physical Surface("Outer_CC")     = {50}; 
Physical Surface("Inner_CC")     = {49}; 

/////////////////// VOLUMES ///////////////////
////// SURFACE LOOPS
// Pre-Flame
Surface Loop(61) = {51,45,46,52,53,54};
// Flame
Surface Loop(62) = {54,47,48,55,56,57};
// Post-Flame
Surface Loop(63) = {57,49,50,58,59,60};

////// VOLUMES
Volume(64) = {61};
Volume(65) = {62};
Volume(66) = {63};

////// LABEL
Physical Volume("Pre-Flame")  = {64};
Physical Volume("Flame")      = {65};
Physical Volume("Post-Flame") = {66};
Physical Volume("Interior")   = {64,65,66};





