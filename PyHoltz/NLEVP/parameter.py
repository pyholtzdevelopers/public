#_____________________________________________________________________
# Copyright (c) 2018 Georg Mensah et al.
# All rights reserved.
#
# Last edited by: 
#
# Date: 
#
# This file is part of PyHoltz.
# PyHoltz is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Lesser General Public License as published by 
# the Free Software Foundation, version 3 of the License. 
#
# PyHoltz is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of 
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License 
# (COPYING.LESSER) along with PyHoltz.
# If not, see <https://www.gnu.org/licenses/>.
# ____________________________________________________________________


import platform
import numpy as np
from PyHoltz.NLEVP.all_algebra import pow0,pow1,pow2,ntau2,factorial #TODO: reimplement algebra

import scipy.sparse as sparse

#import PyHoltz.NLEVP.new_Newton as Newton
#import PyHoltz.NLEVP.Nicoud as Nicoud
#import PyHoltz.NLEVP.Real_Nicoud as Real_Nicoud
#import PyHoltz.NLEVP.Halley as Halley
from PyHoltz.NLEVP import new_Newton as Newton
from PyHoltz.NLEVP import Nicoud as Nicoud
from PyHoltz.NLEVP import Real_Nicoud as Real_Nicoud
from PyHoltz.NLEVP import Halley as Halley

from PyHoltz.NLEVP.inverse_iter import inverse_iter
from PyHoltz.NLEVP.Householder import minres
from scipy.sparse import csr_matrix
from scipy.sparse.linalg import eigs
#from PyHoltz.NLEVP.perturb25 import perturb
from scipy import compress
from scipy.optimize import fsolve
from copy import deepcopy
#from degenerate_perturb import perturb as perturb_devel
from PyHoltz.NLEVP.Beyn import Circ, Polygon
from PyHoltz.NLEVP.Beyn_Multi import Circ_Multi 
from PyHoltz.NLEVP.perturbation_routines import perturb as new_perturb, perturb_fast as new_perturb_fast, perturb_fast_parallel
from PyHoltz.NLEVP.degenerate_perturb import perturb_degenerate


flatten = lambda l: [item for sublist in l for item in sublist]


class para_model:
    ''' Main class for handeling the non-linear eigenvalue problem (NLEVP):
        `L(ω)*p = 0`
        Class enables to set and change parameters. Additionally, inbuild functions
        allow to solve the NLEVP.
        
    '''
#%% Datastructure    
    def __init__(self):
        self.params=[]#['a','b','c']
        self.values=[]#np.array([1,2,3.14])
        self.derivs=[]#np.array([0,0,0])
        self.terms=[]#{'matrix':1,'func':pow1,'parameters':['a']}]
        self.factorials=True
        self.active=['ω']
        self.eigval_param='ω'

    def add_param(self,param,value):
        '''Add parameter and value to non-linear eigenvalue problem.
        '''
        if param in self.params:
            print('WARNING: Parameter "',param,'" already registered! Use the "set_value" and "set_deriv" methods to modify.')
            return
        self.params.append(param)
        self.values.append(value)
        self.derivs.append(0)
        
    def set_value(self,param,value):
        '''Set value of existing parameter.
        '''
        try: 
            idx=self.params.index(param)
        except ValueError:
            print('No parameter named "',param,'" registered! Use the "add_param" method to add to list."' )
            return
        self.values[idx]=value
    def get_value(self,param):
        '''Get value of existing parameter.
        '''
        try: 
            idx=self.params.index(param)
        except ValueError:
            print('No parameter named "',param,'" registered! Use the "add_param" method to add to list."' )
            return
        return self.values[idx]
              
    def set_deriv(self,param,deriv):
        try: 
            idx=self.params.index(param)
        except ValueError:
            print('No parameter named "',param,'" registered! Use the "add_param" method to add to list."' )
            return
        self.derivs[idx]=deriv
        
    def get_deriv(self,param):
        try: 
            idx=self.params.index(param)
        except ValueError:
            print('No parameter named "',param,'" registered! Use the "add_param" method to add to list."' )
            return
        return self.derivs[idx]
        
        
    def add_term(self,term):
        '''Add term (Matirx) to non-linear eigenvalue equation.
        '''
        self.terms.append(term)
    
    def __repr__(self):
        N=self.terms[0]['matrix'].shape[0]
        rpr=str(N)+'x'+str(N)+'Operator: \n \n \t'
        for term in self.terms:
            for fun,pars in zip(term['functions'],term['parameters']):                
                if fun.__name__=='pow1':
                    rpr+=pars[0]+'*'
                elif fun.__name__=='pow2':
                    rpr+=pars[0]+'**2*'
                elif fun.__name__=='tau_delay':
                    rpr+='exp(-i*'+pars[0]+'*'+pars[1]+')*'
                else:    
                    rpr+=fun.__name__+'('
                    for par in pars:
                        rpr+=par+','
                    rpr=rpr[:-1]
                    rpr+=')'
            rpr+=term['operator']+'+'
        rpr=rpr[:-1]
        rpr+='\n \n' + 'Parameters:' + '\t  Values:' + '\n' + '--------------------------- \n'
        for val,par in zip(self.values,self.params):
            rpr+= '\t' + par + '\t\t\t' + str(val) + '\n'
        rpr += '--------------------------- \n'                           
        return rpr        
            
    def __call__(self,*varargin,operator=None,in_or_ex=True):
        nargin=len(varargin)
        nactive=len(self.active)
        shape=self.terms[0]['matrix'].shape
        matrix=0.0*sparse.eye(shape[0],shape[1]) #TODO: Better preallocation
        for term in self.terms:
            if operator!=None:
                if not (term['operator'] in operator) == in_or_ex: #check whether operator-type should be included in operator assembly 
                    continue
                
            coeff=1
            if nargin>nactive: 
                for idx,par in enumerate(self.active):
                    if (not par in flatten(term['parameters']) ) and (varargin[nactive+idx]!=0): #if term does not depend on par derivative is zero
                        coeff=0 #TODO: use a default value possibly diffrent from 0 as derivative order...
                        break
            #evaluate weighting functions
            for fun,params in zip(term['functions'],term['parameters']):
                val=[]
                deriv=[]
                for par in params: #TODO: use list comprehensions
                    if par in self.active:#check whether parameter is an active parameter
                        idx=self.active.index(par)
                        val.append(varargin[idx])
                        if nargin>nactive: # are derivatives specified?
                            deriv.append(varargin[nactive+idx]) 
                        else:
                            deriv.append(0) #TODO: use a default value possibly different from 0
                    else: #use default value if parameter is passive
                        idx=self.params.index(par)
                        val.append(self.values[idx])
                        deriv.append(0) #TODO: use a default value possibly different from 0
                coeff*=fun(*val,*deriv)   
            matrix+=term['matrix']*coeff
        #compute factorials
        if self.factorials:
            fact=1    
            if nactive < nargin:
                for n in varargin[nactive:]:
                    fact*=factorial(n)
            #np.divide(matrix, fact, out=matrix, casting='unsafe')
            matrix/=float(fact)
            #TODO: Use numpy's factorial
        return matrix
        
#%% solve NLEVP        
    def quad2lin(self):
        #features passive interface only
        omega_old=self.get_value('ω')
        self.set_value('ω',1)
        N=self.terms[0]['matrix'].shape[0]
        data=-np.ones(N)
        row=np.arange(N)
        col=np.arange(N)+N
        
        data_m=np.ones(N)
        row_m=np.arange(N)
        col_m=np.arange(N)
        for term in self.terms:
            if not (term['operator'] in ['K','C','M']): 
                continue #TODO: maybe use function and not operator type for decission

            matrix=term['matrix'].tocoo().copy()
            #evaluate coefficient function with eigenvalue set to 1
            coeff=1
            for fun,params in zip(term['functions'],term['parameters']):
                val,deriv=[],[]                    
                for par in params:
                    idx=self.params.index(par)
                    val.append(self.values[idx])
                    deriv.append(0) #TODO: use a default value possibly different from 0
                coeff*=fun(*val,*deriv)
            matrix*=coeff
            #print('operator: ', term['operator'],' coeff: ', coeff,flush=True)            
            
            
            if term['operator'] =='K':
                data=np.hstack((data,matrix.data))
                row=np.hstack((row,matrix.row+N))
                col=np.hstack((col,matrix.col))
                
            elif term['operator']=='C':
                data=np.hstack((data,matrix.data))
                row=np.hstack((row,matrix.row+N))
                col=np.hstack((col,matrix.col+N))

            elif term['operator']=='M':
                data_m=np.hstack((data_m,matrix.data))
                row_m=np.hstack((row_m,matrix.row+N))
                col_m=np.hstack((col_m,matrix.col+N))                
                
        A=csr_matrix((data,(row,col)),shape=(2*N,2*N),dtype=complex)
        M=csr_matrix((data_m,(row_m,col_m)),shape=(2*N,2*N),dtype=complex)
        self.set_value('ω',omega_old)
        return A,M
        
    def quad2linQ(self,omega,m=0):
        N=self.terms[0]['matrix'].shape[0]
        omega_old=self.get_value('ω')
        d_omega_old=self.get_deriv('ω')
        self.set_value('ω',omega)
        self.set_deriv('ω',m)
        for term in self.terms:
            if not (term['operator'] in['Q']):
                continue
            if ['b'] in term['parameters']:
                continue
            matrix=term['matrix'].tocoo().copy()
            #evaluate coefficient function with eigenvalue set to 1
            coeff=1
            for fun,params in zip(term['functions'],term['parameters']):
                val,deriv=[],[]                    
                for par in params:
                    idx=self.params.index(par)
                    val.append(self.values[idx])
                    deriv.append(self.derivs[idx]) #TODO: use a default value possibly different from 0
                coeff*=fun(*val,*deriv)
            data=matrix.data*coeff
            row=matrix.row+N
            col=matrix.col
        Q=csr_matrix((data+0j,(row,col)),shape=(2*N,2*N))
        self.set_value('ω',omega_old)
        self.set_deriv('ω',d_omega_old)
        return Q
    
    def initialize_nicoud(self,omega):
        #initialize
        A,M=self.quad2lin()
        ew,ev=eigs(A=A,M=M,k=5, sigma=omega)
        print(ew/2/np.pi)
        
        
    def nicoud(self,omega_target,maxiter=15,tol=0,relax=1,n_eig_val=3,v0=[],output=False):
        '''Solve NLEVP with Nicoud's method :func:`NLEVP.Real_Nicoud.iteration` .
            
        '''
        #%% set up quadratic system
        A,M=self.quad2lin()
        Q=lambda omega,m=0:self.quad2linQ(omega,m=m)+pow0(omega,m)*A
        self.omega,self.p,self.p_adj,self.iterations,self.flag=Real_Nicoud.iteration(Q,M,omega_target,maxiter=maxiter,tol=tol,relax=relax,n_eig_val=n_eig_val,output=output)
        #TODO: check contraction here
        #downsize system
        N=A.shape[0]//2
        self.p=self.p[:N,:]
        self.p_adj=self.p_adj[:N,:]
                
    def newt(self,omega_target,maxiter=15,tol=0,relax=1,n_eig_val=3,v0=[],output=True,purge=False,tol_lamb=1e-8):
        '''Solve NLEVP with Newton's method :func:`NLEVP.Newton.iteration` .
            
        '''
        self.omega,self.p,self.p_adj,self.iterations = Newton.iteration(L=lambda om, m=0: self(om,m),k=omega_target,maxiter=maxiter,tol=tol,relax=relax,n_eig_val=n_eig_val,v0=v0,output=output,purge=purge,tol_lamb=tol_lamb)
        #TODO: Implement keywordless usage of L in Newton

    def householder(self,omega_target,maxiter=15,tol=1e-16,relax=1.,lam_tol=1e-8,order=1,n_eig_val=1,v0=[],output=False,printRes=True):
        '''Solve NLEVP with Householder method :func:`NLEVP.Householder.minres` .
        '''
        self.eigval_param='λ'
        #print('Activity test 1:',self.active,flush=True)
        #active=self.active
        #self.active=['λ','ω']
        if hasattr(self,'omega_pert'):
            omega_pert=self.omega_pert.copy()
        else:
            omega_pert=False
            
        if hasattr(self,'p_pert'):
            p_pert=self.p_pert.copy()
        else:
            p_pert=False
        #print('Activity test 2:',self.active,flush=True)    
        self.omega,self.p,self.p_adj,self.iterations,self.flag = minres(L=self,z0=omega_target,maxiter=maxiter,tol=tol,relax=relax,lam_tol=lam_tol,order=order,n_eig_val=n_eig_val,v0=v0,output=output,printRes=printRes)
        self.eigval_param='ω'

        if omega_pert:
            self.omega_pert=omega_pert
        
        if p_pert:
            self.p_pert=p_pert
    
    def inverse_iter(self,omega_target,v0=[],maxiter=15,tol=0,v=[],output=False):
        '''Solve NLEVP with inverse iteration method :func:`NLEVP.inverse_iter.inverse_iter`.
          
          Parameters
          ----------
          
          omega_target : float or complex
                         initial guess for the eigenvalue
                         
          v0 : array_like, optional 
               initial guess for the eigenvector (defaul numpy.ones())
               
          maxiter : int, optional
                    maximum number of iterations (default 15)
          
          tol : float, optional
                tolerance (default 0)
                    
          v : array_like, optional 
              normalization vector (if not specified the normalization vector is
              set to v0)         
        '''
        if v0 == []:
            v0 = np.matlib.ones((self(omega_target).shape[0],1))
        self.omega,self.p,self.iterations,self.flag = inverse_iter(self,omega_target,v0,maxiter,tol,v,output)             
    def fix(self,omega_target,maxiter=15,tol=0,relax=1,n_eig_val=3,v0=[],output=False):        
        self.omega,self.p,self.p_adj,self.iterations,self.flag = Nicoud.iteration(lhs=lambda om : -1*self(om,operator=['M'],in_or_ex=False),rhs=lambda om: self(1,operator=['M'],in_or_ex=True),k=omega_target,maxiter=maxiter,tol=tol,relax=relax,n_eig_val=n_eig_val,v0=v0,output=output)

    def halley(self,omega_target,maxiter=15,tol=0,relax=1,n_eig_val=3,v0=[],output=False):
        '''Solve NLEVP with Halley's method :func:`NLEVP.Halley.iteration`.
          
          Parameters
          ----------
          
          omega_target : float or complex
                         initial guess for the eigenvalue
          
          maxiter : int, optional
                    maximum number of iterations (default 15)
          
          tol : float, optional
                    tolerance (default 0)

          
          relax : float, optional
                    relaxation factor (default 1)
          
          n_eig_val : int, optional
                    number of eigenvalues (default 3)
                    
          v0 : array_like, optional 
               initial guess for the eigenvector
        '''
        self.omega,self.p,self.p_adj,self.iterations = Halley.iteration(L=lambda om, m=0 : self(om,m),k=omega_target,maxiter=maxiter,tol=tol,relax=relax,n_eig_val=n_eig_val,v0=v0,output=output)


    def beyn(self,z0,R,l=25,N=25,tol_rank=1E-10,info=False):
        self.omega,self.p,self.rankTestFlag=Circ(self.__call__,z0,R=R,l=l,N=N,tol_rank=tol_rank,info=info)

    def beyn_poly(self,vert,l=25,N=25,h=float('inf'),tol_rank=1E-10,info=False):
        self.omega,self.p,self.rankTestFlag=Polygon(self.__call__,vert,l=l,N=N,h=h,tol_rank=tol_rank,info=info)

        
    def beyn_multi(self,z0,R,K=10,l=25,N=25,tol_rank=1E-10):#info=False):
        self.omega,self.p,self.rankTestFlag=Circ_Multi(self.__call__,z0,R=R,l=l,N=N,tol_rank=tol_rank,K=K)
    
    def solve(self,omega_target,maxiter=0,tol=0,stol=1e-10,info=False):
        dispersion =lambda omega: np.linalg.det(self.__call__(omega))#.todense())
        
        def dispers(x):
            omega=x[0]+x[1]*1j
            disp=dispersion(omega)
            #print(disp)
            return [np.real(disp),np.imag(disp)]
        
        x, infodict, flag, mesg = fsolve(dispers,[omega_target.real, omega_target.imag],full_output=1,xtol=tol,maxfev=maxiter)#,args=(0,0))
        if info:
            print('Frequency: ',(x[0]+x[1]*1j)/2/np.pi,' Residue: ',np.abs(dispersion(x[0]+x[1]*1j)),flush=True)
        #val=[[x[0]/2/np.pi,x[1]/2/np.pi]]
        if flag==1:
            self.flag=1
            #self.omega=np.array(x[0]+x[1]*1j)
        elif flag==2:
            self.flag=-1
        else:
            self.flag=-3
            #self.omega=float(NaN)
        self.iterations=infodict['nfev']
        self.omega=np.array(x[0]+x[1]*1j)
        self.msg=mesg
        
        u, s, vh = np.linalg.svd(self.__call__(self.omega))#.todense())
        null_mask = (s <= stol)
        #TODO: consistenly use conjugation
        null_space = compress(null_mask, u, axis=1)
        self.p_adj=np.matrix(null_space)       
        null_space = compress(null_mask, vh, axis=0)
        self.p=np.matrix(null_space.conj().T) 
#%% compute perturbation         
    def perturb(self,epsilon,N=5):
        active=self.active
        self.active=['ω',epsilon]
        epsilon0=self.get_value(epsilon)
        self.omega_pert,self.p_pert=perturb(self.__call__,self.omega,epsilon0,self.p,self.p_adj,N)            
        self.active=active
        self.epsilon=epsilon
        
    def new_perturb(self,epsilon,N=5):
        active=self.active
        self.active=[self.eigval_param,epsilon]
        omega0=self.omega
        epsilon0=self.get_value(epsilon)
        
        def L(m,n):
            return self.__call__(omega0,epsilon0,m,n)
        self.omega_pert,self.p_pert=new_perturb(L ,self.p,self.p_adj,N)
        self.omega_pert[0]=omega0.item(0)            
        self.active=active
        self.epsilon=epsilon
        
    def new_perturb_fast(self,epsilon,N=5,printOrd=True):
        active=self.active
        self.active=[self.eigval_param,epsilon]
        omega0=self.omega
        epsilon0=self.get_value(epsilon)
        self.epsilon=epsilon
        
        def L(m,n):
            return self.__call__(omega0,epsilon0,m,n)
        self.omega_pert,self.p_pert=new_perturb_fast(L ,self.p,self.p_adj,N,printOrd=printOrd)
        self.omega_pert[0]=omega0.item(0)            
        self.active=active  
        self.epsilon=epsilon
        
    def perturb_parallel(self,epsilon,N=5,normalize=True,info=False,printOrd=True):
        active=self.active
        self.active=[self.eigval_param,epsilon]
        omega0=self.omega
        epsilon0=self.get_value(epsilon)
        
        def L(m,n):
            return self.__call__(omega0,epsilon0,m,n)
        
        if platform.system()=='Windows':
            print('Warning: Can not use parallelization on Windows machines.')
            self.omega_pert,self.p_pert=new_perturb_fast(L ,self.p,self.p_adj,N,printOrd=printOrd)
        else:
            self.omega_pert,self.p_pert=perturb_fast_parallel(L ,self.p,self.p_adj,N,normalize,info=info,printOrd=printOrd)
        self.omega_pert[0]=omega0.item(0)            
        self.active=active  
        self.epsilon=epsilon
        
    def perturb_degenerate(self,epsilon,N=2,rtol=100,issparse=True,debug=False,normalisation=False):
        active=self.active
        self.active=['ω',epsilon]
        epsilon0=self.get_value(epsilon)
        self.Result=perturb_degenerate(self.__call__,self.omega,epsilon0,self.p,self.p_adj,N,rtol=rtol,issparse=issparse,debug=debug,normalisation=normalisation)
        self.active=active
        self.epsilon=epsilon

        #%% perturbation aftermath   
    def solution(self):
        if  self.p.shape[1] == 1:
            sol=NLEVP_solution()
            sol.params=self.params.copy()
            sol.values=self.values.copy()
            
            if hasattr(self,'omega'): sol.omega=self.omega.copy()
            if hasattr(self,'omega_pert'): sol.omega_pert=self.omega_pert.copy()
            if hasattr(self,'p_pert'): sol.p_pert=self.p_pert.copy()
            if hasattr(self,'epsilon'): 
                sol.epsilon=self.epsilon
                sol.eps0=self.get_value(sol.epsilon)
        else:
            #degenerate_Case
            sol=[]
            order = len(self.Result[1])-1
            branches = [ i for i in self.Result[0].keys() if len(i)==order]

            for nB, branch in enumerate(branches):
                solTemp = NLEVP_solution()
                solTemp.params=self.params.copy()
                solTemp.values=self.values.copy()
                
                if hasattr(self,'omega'): solTemp.omega=self.omega.copy()
                
                oms = []
                ps = []
                for i in range(order+1):
                    oms += [self.Result[0][branch[0:i]]]
                    ps += [self.Result[1][i][branch].copy()]
                    
                solTemp.omega_pert=oms
                solTemp.p_pert=ps
                
                if hasattr(self,'epsilon'): 
                    solTemp.epsilon=self.epsilon
                    solTemp.eps0=self.get_value(solTemp.epsilon)
                
                sol += [solTemp]
            
        return sol

class NLEVP_solution:
    def __init__(self):
        self.omega_pert=[]
        self.p_pert=[]
        self.params=[]
        self.values=[]
        self.epsilon=''
        self.omega=float('NaN')
        self.eps0=float('NaN')
 
    def normalize_pert(self):
        v=deepcopy(self.p_pert)
        for k in range(1,10):
            #print('k:',k)
            s=0.+0.j
            for l in range(1,k):
                s+=v[l].H*v[k-l]
            #print('s: ',s,'inner ',v[0].H*v[k],'norm:', np.linalg.norm(v[k]))
            v[k]-=v[0]*(s/2.+v[0].H*v[k])
        self.v =v
        
    def estimate_pol(self):
        lam=deepcopy(self.omega_pert)
        eps_sng=[]
        a=[]
        radius=[]
        for j in range(1,len(lam)-1):
            eps_sng+=[(lam[j]*lam[j-1])/( (j+1)*lam[j+1]*lam[j-1]-j*lam[j]**2)]
            radius+=[np.abs(eps_sng[j-1])]
            a+=[((j**2-1)*lam[j+1]*lam[j-1]-(j*lam[j])**2)/( (j+1)*lam[j+1]*lam[j-1]-j*lam[j]**2)]
        self.conv_radius=radius
        self.pole_order=a
        self.pole=eps_sng#+self.eps0
        
    def poly(self,val,N):
        order = len(self.omega_pert)-1
        if N>order:
            print('Warning: expansion was performed only up to order '+str(order)+'. Returning the highest known expansion.')    
        val=val-self.eps0
        return np.polyval(np.array(self.omega_pert)[N::-1],val)     
    
    def polyV(self,val,N):
        order = len(self.omega_pert)-1
        if N>order:
            print('Warning: expansion was performed only up to order '+str(order)+'. Returning the highest known expansion.')       
        val=val-self.eps0
        return np.polyval(self.v[N::-1],val)
    
    def polyP(self,val,N):
        order = len(self.omega_pert)-1 
        if N>order:
            print('Warning: expansion was performed only up to order '+str(order)+'. Returning the highest known expansion.')   
        val=val-self.eps0
        return np.polyval(self.p_pert[N::-1],val)
                
#%%
#model=para_model()
#model.add_param('a',0)

#model.eval_terms()
#model.set_value('a',3)
#model.eval_terms()
#model.set_value('y',2)
#model.set_deriv('a',1)
#model.eval_terms()     
#model.set_deriv('b',0)       
#model.eval_terms()            
        
