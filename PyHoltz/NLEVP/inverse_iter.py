#_____________________________________________________________________
# Copyright (c) 2018 Georg Mensah et al.
# All rights reserved.
#
# Last edited by: 
#
# Date: 
#
# This file is part of PyHoltz.
# PyHoltz is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Lesser General Public License as published by 
# the Free Software Foundation, version 3 of the License. 
#
# PyHoltz is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of 
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License 
# (COPYING.LESSER) along with PyHoltz.
# If not, see <https://www.gnu.org/licenses/>.
# ____________________________________________________________________
"""
Created on Mon Jun  4 14:17:35 2018

@author: jakob
"""

import numpy as np
import scipy.sparse.linalg as linalg

def inverse_iter(L,z1,x0,maxiter=10,tol=0.,v=[],output=True):
    '''
    Inverse Iteration algorithm (variant of Newton's method) for 
    solving non-linear eigenvalue problems according to Mehrmann [2]_.

    The eigenvalue problem considered reads as follows: ``L(z1)*x0 = 0`` 
    with the normalization criterion: ``v.H*x0 - 1 = 0``.
    
    Parameters 
    ----------
    L : function, callable object
        operator family
        
    z1 : float or complex
        initial guess for the eigenvalue
    
    x0 : array_like
        initial guess for the eigenvector
    
    maxiter : int, optional
        maximum number of iterations (default is ``10``) 
        
    tol : float
        tolerance. Convergence is assumend  when: 
        ``norm(L(z1)*x0))**2 + (v.H*x0)**2 < tol**2``
            
    v : array_like, optional 
        normalization vector (if not specified the normalization vector is
        set to x0)
    
    Returns
    -------
     z1 : matrix
        eigenvalue
            
    x0 : matrix
        eigenvector
    
    n : int
        number of iterations
    
    flag : int
          iteration flag
        
    Notes
    -----
    Method is gradient based.
            
                    
    ..  [2] V. Mehrmann, H. Voss. 2004. Nonlinear eigenvalue problems: 
        a challenge for modern eigenvalue methods. GAMM-Mitt. 27(2):121–152
    '''
    if output: 
        print("Launching Inverse Iteration...")
        print("Iter    Res:      z:")
        print("----------------------------------")
    
    if v == []:
        v = x0
    x0/= (v.H*x0).item(0)
    
    z0=float('inf')
    n = 0
    res=np.linalg.norm(L(z1)*x0)**2+((v.H*x0).item(0)-1)**2
    
    try:
        while res>tol**2 and n<maxiter:
            if output: print(n,np.sqrt(res), z1 )
    
            u = np.matrix(linalg.spsolve(L(z1),L(z1,1)*x0)).T
            z0 = z1
            z1-= (v.H*x0/(v.H*u)).item(0)
            x0 = u/(v.H*u)
            res=np.linalg.norm(L(z1)*x0)**2+((v.H*x0).item(0)-1)**2
            n+=1
        
    except Exception as excp:
        if output:
            print('Error occured:')
            print(excp)
            print('...aborted Inverse Iteration!',flush=True)
        flag=-2
    
    if n>=maxiter:
        flag=-1
        if output: print('Warning: Maximum number of iterations has been reached!')
    elif res<= tol**2:
        flag = 1
        if output: print('Solution has converged!')
    else:
        if output: print('Warning: This should not be possible....\n If you can read this contact GAM!')
        flag=-3

    if output: print('...finished Inverse Iteration!',flush=True)

    return np.array(z1), np.matrix(x0), n, flag




















