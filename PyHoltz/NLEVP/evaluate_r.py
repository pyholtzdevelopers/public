#_____________________________________________________________________
# Copyright (c) 2018 Georg Mensah et al.
# All rights reserved.
#
# Last edited by: 
#
# Date: 
#
# This file is part of PyHoltz.
# PyHoltz is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Lesser General Public License as published by 
# the Free Software Foundation, version 3 of the License. 
#
# PyHoltz is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of 
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License 
# (COPYING.LESSER) along with PyHoltz.
# If not, see <https://www.gnu.org/licenses/>.
# ____________________________________________________________________
"""
Created on Tue Feb 27 11:11:56 2018

@author: mensah
"""

from scipy.special import factorial
import numpy as np
import pickle 
from PyHoltz.NLEVP.perturbation_routines import MN


def accel_asc(n):
#Kellehrer's Algorithm
    a = [0 for i in range(n + 1)]
    k = 1
    y = n - 1
    while k != 0:
        x = a[k - 1] + 1
        k -= 1
        while 2 * x <= y:
            a[k] = x
            y -= x
            k += 1
        l = k + 1
        while x <= y:
            a[k] = x
            a[l] = y
            yield a[:k + 2]
            x += 1
            y -= 1
        a[k] = x + y
        y = x + y - 1
        yield a[:k + 1]    

        
def umordnen(multi):
    z = np.sum(multi)
    n = np.zeros(z,dtype='int64')
    if multi != [0]:
        for i in multi:
            n[i-1] = n[i-1]+1
        return n
    else:
        return np.zeros(0,dtype='int64')

def multinomcoeff(mu):
    coeff=factorial(np.sum(mu))/np.prod(factorial(mu))
    coeff=coeff.item(0)
    return coeff


def calculate_Mv(L,p,branch,lam,v,orderv,branchv):

    M = L(0,p)*v[orderv][branchv]    
    
    for m in range(1,p+1):
        for mu in accel_asc(m):
            mu = umordnen(mu)
            coeff=1.
            for g,mu_g in enumerate(mu):
                if branch[0:g+1] == ():
                    print('You are trying to calculate the nth order M term \
                    before calculating the nth order eigenvalue. \
                    You cannot.')
                    exit            
                coeff*=lam[branch[0:g+1]]**mu_g        
                          
            coeff*=multinomcoeff(mu)   
            M+=L(int(np.sum(mu)),p-m)*v[orderv][branchv]*coeff    
                
    return -M


def calculate_r(L,k,v,branch,lam):
    #compute r_k
    #v[idx] = eigenvector at order x
    #lam[g] = eigenvalue at order g
    
    r=v[0][branch]*0 

    for n in range(1,k+1):
        r+=L(0,n)*v[k-n][branch]
#        print('|L_{0,'+str(n)+'}|')
#        print(np.linalg.norm(L(0,n)))
#        print('|L_{0,'+str(n)+'}*v_{'+str(k-n)+'}|')
#        print(np.linalg.norm(L(0,n)*v[k-n][branch]))
    
    for m in range(1,k+1):
        for mu in accel_asc(m):
            if mu==[k]:
                continue
            mu = umordnen(mu)
            for n in range(0,k-m+1):
                
                coeff=1.
                for g,mu_g in enumerate(mu):
                    coeff*=lam[branch[0:g+1]]**mu_g
                              
                coeff*=multinomcoeff(mu)

                r+=L(int(np.sum(mu)),n)*v[k-m-n][branch]*coeff
                    
#                print('|L_{'+str(np.sum(mu))+','+str(n)+'}|')
#                print(np.linalg.norm(L(int(np.sum(mu)),n)))
#                print('v_{'+str(k-m-n)+'}|')                
#                print(np.linalg.norm(v[k-m-n][branch]))
#                print('|L_{'+str(np.sum(mu))+','+str(n)+'}*v_{'+str(k-m-n)+'}|')                
#                print(np.linalg.norm(L(int(np.sum(mu)),n)*v[k-m-n][branch]))
#                print(' ')

    return r




#%%
#MN = []
#for i in range(31):
#    with open("NLEVP/perturbation_data/L_order_"+str(i)+".pickle", "rb") as fp:   # Unpickling
#        MN.append(pickle.load(fp))

def weigh(mu):
    weight=0
    for g,mu_g in enumerate(mu):
        weight+=(g+1)*mu_g
    return weight

def calculate_r_fast(L,N,v,branch,lam):
    for k in range(1,N+1):
        r=np.zeros(v[0][branch].shape,dtype=complex)
        for (m,n) in MN[k]:
            w=np.zeros(v[0][branch].shape,dtype=complex)
            for mu in MN[k][(m,n)]:
                coeff=1.
                for g,mu_g in enumerate(mu):
                    coeff*=lam[branch[0:g+1]]**mu_g
                w+=v[k-n-weigh(mu)][branch]*multinomcoeff(mu)*coeff
            r+=L(m,n)*w
    return r
    

##%%
#k=1 #order
##compute r_k
#r=p*0 
#for n in range(1,k+1):
#    r+=L(0,n)*v[k-n]
#
#for m in range(1,k):
#    for mu in accel_asc(m):
#        mu = umordnen(mu)
#        for n in range(0,k-m+1):
#            coeff=1.
#            for g,mu_g in enumerate(mu):
#                coeff*=lam[g+1]**mu_g
#            coeff*=multinomcoeff(mu)    
#            r+=L(np.sum(mu),n)v[k-m-n]*coeff
