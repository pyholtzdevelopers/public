#_____________________________________________________________________
# Copyright (c) 2018 Georg Mensah et al.
# All rights reserved.
#
# Last edited by: 
#
# Date: 
#
# This file is part of PyHoltz.
# PyHoltz is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Lesser General Public License as published by 
# the Free Software Foundation, version 3 of the License. 
#
# PyHoltz is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of 
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License 
# (COPYING.LESSER) along with PyHoltz.
# If not, see <https://www.gnu.org/licenses/>.
# ____________________________________________________________________
"""
Created on Fri Dec  2 14:02:39 2016

@author: georg
"""

 # -*- coding: utf-8 -*-
# Georg Mensah, Philip Buschmann, Jonas Moeck, TU Berlin 2015
"""
Created on Fri Dec  4 16:21:54 2015

@author: georg
"""
import math as math
import numpy as np
import scipy.sparse.linalg as linalg
from scipy.sparse import identity
def iteration(L,k,maxiter=10,tol=0,relax=1,n_eig_val=3,v0=[],output=True):
    ''' Utilizes Halley's method for the solution of 
    non-linear eigenvalue problems.
    
    The eigenvalue problem considered reads as follows:
    `L(k)*v =0`
    
    Parameters
    ----------
    
    L : function, callable object
        operator family
        
    k : float or complex
        initial guess for the eigenvalue
          
    maxiter : int, optional
        maximum number of iterations (default is 10) 
        
    tol : float
        tolerance. Convergnece is assumend  when `abs(k_n-k_{n-1})<tol`

    relax : float, optional   
        relaxation parameter between 0 and 1. (This feature is 
        experimental, the default is 1, which implies no relaxation )
            
    v0 : array_like, optional 
         initial guess for the eigenvector (if not specified the vector is 
         initialized usings ones only)
           
    Returns
    -------
    
    k : matrix
        eigenvalue
            
    v : matrix
        eigenvector
        
    v_adj : matrix
        adjoint eigenvector
            
    Notes
    -----
        In case of mode degenracy the vectors returned feature multiple 
        columns.

    '''
    if output: print("Launching Halley...")
    k0=float('inf')
    k_n=[k]
    iteration=0
    
    if v0==[]:
        v0=np.ones((L(k).shape[0],1))
    ident=identity(len(v0))
    while abs(k0-k)>tol and iteration<maxiter:
        if output: print("Iteration: ",iteration, ", Res:",abs(k0-k),',Freq',k/2/math.pi)
        k0=k
        A=L(k0)
        k,v = linalg.eigs(A=A, k=n_eig_val, sigma = 0,v0=v0)
        delta_k=abs(k)         
        index=np.argsort(delta_k)
        k=k[index]
        v=np.matrix(v[:,index])
        #k=k[index[0]]
        #v0=np.matrix(v[:,index[0],np.newaxis]) #TODO: consider relxation on vector
        #1.compute adjoint solution        
        k_adj,v_adj = linalg.eigs(A=(A).H, k=n_eig_val, sigma = 0)
        delta_k_adj=abs(k_adj)  
        index_adj=np.argsort(delta_k_adj) #Todo degenerate theory
        v_adj=np.matrix(v_adj[:,index_adj])
        #v_adj=np.matrix(v_adj[:,index_adj[0],np.newaxis])
        #compute derivative
        #derivative is taken at k0!
        #deriv=(v_adj.H*L(k0,m=1)*v0)/(v_adj.H*v0)
        #deriv2=(v0.H*(L(k0,m=0).T*L(k0,m=1)+L(k0,m=1).T*L(k0,m=0))*v0)/(v0.H*v0)/(2*k)
        #print('test: ', deriv,'number2 :',  deriv2 , 'ratio: ', abs(deriv/deriv2), 'endtest')
        more=[]
        k_adj=k_adj[index_adj]
        for i in range(n_eig_val):
            rhs=L(k0,m=1)*v[:,i]
            deriv=(v_adj[:,i].H*rhs)/(v_adj[:,i].H*v[:,i])
            deriv=deriv.item(0)
            #stop here to get newton
            #more.append((relax*(k0-k[i]/deriv)+(1-relax)*k0).item(0))       
            mode_deriv=np.matrix(linalg.spsolve(A-k[i]*ident,-rhs+deriv*v[:,i])).T            
            rhs=L(k0,m=1)*mode_deriv+L(k0,m=2)*v[:,i]-deriv*mode_deriv
            second_deriv=2*(v_adj[:,i].H*rhs)/(v_adj[:,i].H*v[:,i]) 
            more.append((relax*(k0-2*k[i]*deriv/(2*deriv**2-k[i]*second_deriv))+(1-relax)*k0).item(0))
            #print(i,'. ', k2[i], ' vs. ',k_adj2[i])
        more=np.array(more)
        index=np.argsort(abs(more-k0)) #choose closest to k0
                    
        #k=relax*(k0-k/deriv)+(1-relax)*k0    
        
        k=more[index[0]].item(0)        
        iteration+=1
        k_n.append(k)
        #print(np.linalg.norm(lhs(k0)*v0-rhs(k0)*v0*k0**2))

    #print(delta_k,tol,delta_k[index[1]])
    #TODO degnerate
    #if delta_k[index[1]]<=(tol*10):
    #    #print('juhu',v0.shape)
    #    v0=v[:,index[0:2]]
        #print('juhu',v0.shape)
        
    #Compute adjoint solution        
    k_adj,v_adj = linalg.eigs(A=(L(k0)).H, k=3, sigma = 0)
    delta_k_adj=abs(k_adj)  
    #print(delta_k_adj)
    index=np.argsort(delta_k_adj)
    #if delta_k_adj[1]<=tol*10:
    #    v_adj=v_adj[:,index[0:2]]
    #else:
    v_adj=v_adj[:,index[0],np.newaxis]
        
            
    if output: 
        print("Iterations: ",iteration, ", Res:",abs(k0-k),',Freq',k/2/math.pi)
        print("... Halley finished!")
    #print('RES:', abs(k0-k))
    return np.array(k),np.matrix(v0),np.matrix(v_adj),iteration
        
        