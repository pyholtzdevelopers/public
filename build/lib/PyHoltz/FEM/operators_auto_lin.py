#_____________________________________________________________________
# Copyright (c) 2018 Georg Mensah et al.
# All rights reserved.
#
# Last edited by: 
#
# Date: 
#
# This file is part of PyHoltz.
# PyHoltz is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Lesser General Public License as published by 
# the Free Software Foundation, version 3 of the License. 
#
# PyHoltz is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of 
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License 
# (COPYING.LESSER) along with PyHoltz.
# If not, see <https://www.gnu.org/licenses/>.
# ____________________________________________________________________


from PyHoltz.FEM.mesh_utils import smplx_finder, find_tetrahedron_containing_x
import numpy as np
#%%Mass operator
def assemble_mass_operator(points,tets, lines=None):
    '''
        mass operator
    '''
    # Coordinates and number of points
    x = points[:,0]
    y = points[:,1]
    z = points[:,2]
    
    N = len(x)
    
    # Allocate the necessary space

    ii = np.zeros((len(tets),4**2),dtype=int)
    jj = np.zeros((len(tets),4**2),dtype=int)
    mm = np.zeros((len(tets),4**2))
    jj_loc = np.repeat(np.r_[0:4],4)
    jj_loc = np.reshape(jj_loc,(4,4))
    ii_loc = np.transpose(jj_loc)
    ii_loc = ii_loc.flatten()
    jj_loc = jj_loc.flatten()
    # The constant local mass matrix
    m_loc = np.matrix([            [1/60, 1/120, 1/120, 1/120],
            [1/120, 1/60, 1/120, 1/120],
            [1/120, 1/120, 1/60, 1/120],
            [1/120, 1/120, 1/120, 1/60],
                ])

    # Loop over all tetrahedra  
    for k, tet in enumerate(tets):
        # Ten nodes (4 coreners 6 edges) for the local element
        nodes = []
        nodes += [tet[0], tet[1], tet[2], tet[3]]
        nodes = np.array(nodes)


        #local coordinate transformation        
        J = np.matrix([
            [x[tet[1]]-x[tet[0]], x[tet[2]]-x[tet[0]], x[tet[3]]-x[tet[0]]], 
            [y[tet[1]]-y[tet[0]], y[tet[2]]-y[tet[0]], y[tet[3]]-y[tet[0]]],
            [z[tet[1]]-z[tet[0]], z[tet[2]]-z[tet[0]], z[tet[3]]-z[tet[0]]]])
        
        # Assmeble the entries
        m = m_loc*np.abs(np.linalg.det(J))
        
        ii[k,:] = nodes[ii_loc]
        jj[k,:] = nodes[jj_loc]
        
        mm[k,:] = m[ii_loc, jj_loc]
    
    return mm.flatten(), ii.flatten(), jj.flatten()


#%% Stiffness operator
def assemble_stiffness_operator(points, cfield, tets, lines):
    '''
        stiffness operator
    '''
    x = points[:,0]
    y = points[:,1]
    z = points[:,2]
    
    N = len(x)

    ii = np.zeros((len(tets),4**2),dtype=int)
    jj = np.zeros((len(tets),4**2),dtype=int)
    mm = np.zeros((len(tets),4**2))
    jj_loc = np.repeat(np.r_[0:4],4)
    jj_loc = np.reshape(jj_loc,(4,4))
    ii_loc = np.transpose(jj_loc)
    ii_loc = ii_loc.flatten()
    jj_loc = jj_loc.flatten()

    m_loc = np.zeros((4,4))


    # Loop over all tetrahedra  
    for k, tet in enumerate(tets):
        # Ten nodes (4 coreners 6 edges) for the local element
        nodes = []
        nodes += [tet[0], tet[1], tet[2], tet[3]]
        nodes = np.array(nodes)        

        #compute c**2 functions
        c = np.outer(cfield[nodes[0:4]], cfield[nodes[0:4]])           


        #local coordinate transformation        
        J = np.matrix([
            [x[tet[1]]-x[tet[0]], x[tet[2]]-x[tet[0]], x[tet[3]]-x[tet[0]]], 
            [y[tet[1]]-y[tet[0]], y[tet[2]]-y[tet[0]], y[tet[3]]-y[tet[0]]],
            [z[tet[1]]-z[tet[0]], z[tet[2]]-z[tet[0]], z[tet[3]]-z[tet[0]]]])
        
        A=J.I*J.I.T
        #A=A.T

        # Local stiffness matrix generated via Sympy
        m_loc[0,0]=-A[0,0]*c[0,0]/60 - A[0,0]*c[0,1]/60 - A[0,0]*c[0,2]/60 - A[0,0]*c[0,3]/60 - A[0,0]*c[1,1]/60 - A[0,0]*c[1,2]/60 - A[0,0]*c[1,3]/60 - A[0,0]*c[2,2]/60 - A[0,0]*c[2,3]/60 - A[0,0]*c[3,3]/60 - A[0,1]*c[0,0]/30 - A[0,1]*c[0,1]/30 - A[0,1]*c[0,2]/30 - A[0,1]*c[0,3]/30 - A[0,1]*c[1,1]/30 - A[0,1]*c[1,2]/30 - A[0,1]*c[1,3]/30 - A[0,1]*c[2,2]/30 - A[0,1]*c[2,3]/30 - A[0,1]*c[3,3]/30 - A[0,2]*c[0,0]/30 - A[0,2]*c[0,1]/30 - A[0,2]*c[0,2]/30 - A[0,2]*c[0,3]/30 - A[0,2]*c[1,1]/30 - A[0,2]*c[1,2]/30 - A[0,2]*c[1,3]/30 - A[0,2]*c[2,2]/30 - A[0,2]*c[2,3]/30 - A[0,2]*c[3,3]/30 - A[1,1]*c[0,0]/60 - A[1,1]*c[0,1]/60 - A[1,1]*c[0,2]/60 - A[1,1]*c[0,3]/60 - A[1,1]*c[1,1]/60 - A[1,1]*c[1,2]/60 - A[1,1]*c[1,3]/60 - A[1,1]*c[2,2]/60 - A[1,1]*c[2,3]/60 - A[1,1]*c[3,3]/60 - A[1,2]*c[0,0]/30 - A[1,2]*c[0,1]/30 - A[1,2]*c[0,2]/30 - A[1,2]*c[0,3]/30 - A[1,2]*c[1,1]/30 - A[1,2]*c[1,2]/30 - A[1,2]*c[1,3]/30 - A[1,2]*c[2,2]/30 - A[1,2]*c[2,3]/30 - A[1,2]*c[3,3]/30 - A[2,2]*c[0,0]/60 - A[2,2]*c[0,1]/60 - A[2,2]*c[0,2]/60 - A[2,2]*c[0,3]/60 - A[2,2]*c[1,1]/60 - A[2,2]*c[1,2]/60 - A[2,2]*c[1,3]/60 - A[2,2]*c[2,2]/60 - A[2,2]*c[2,3]/60 - A[2,2]*c[3,3]/60
        m_loc[0,1]=A[0,0]*c[0,0]/60 + A[0,0]*c[0,1]/60 + A[0,0]*c[0,2]/60 + A[0,0]*c[0,3]/60 + A[0,0]*c[1,1]/60 + A[0,0]*c[1,2]/60 + A[0,0]*c[1,3]/60 + A[0,0]*c[2,2]/60 + A[0,0]*c[2,3]/60 + A[0,0]*c[3,3]/60 + A[0,1]*c[0,0]/60 + A[0,1]*c[0,1]/60 + A[0,1]*c[0,2]/60 + A[0,1]*c[0,3]/60 + A[0,1]*c[1,1]/60 + A[0,1]*c[1,2]/60 + A[0,1]*c[1,3]/60 + A[0,1]*c[2,2]/60 + A[0,1]*c[2,3]/60 + A[0,1]*c[3,3]/60 + A[0,2]*c[0,0]/60 + A[0,2]*c[0,1]/60 + A[0,2]*c[0,2]/60 + A[0,2]*c[0,3]/60 + A[0,2]*c[1,1]/60 + A[0,2]*c[1,2]/60 + A[0,2]*c[1,3]/60 + A[0,2]*c[2,2]/60 + A[0,2]*c[2,3]/60 + A[0,2]*c[3,3]/60
        m_loc[1,0]=m_loc[0,1]
        m_loc[0,2]=A[0,1]*c[0,0]/60 + A[0,1]*c[0,1]/60 + A[0,1]*c[0,2]/60 + A[0,1]*c[0,3]/60 + A[0,1]*c[1,1]/60 + A[0,1]*c[1,2]/60 + A[0,1]*c[1,3]/60 + A[0,1]*c[2,2]/60 + A[0,1]*c[2,3]/60 + A[0,1]*c[3,3]/60 + A[1,1]*c[0,0]/60 + A[1,1]*c[0,1]/60 + A[1,1]*c[0,2]/60 + A[1,1]*c[0,3]/60 + A[1,1]*c[1,1]/60 + A[1,1]*c[1,2]/60 + A[1,1]*c[1,3]/60 + A[1,1]*c[2,2]/60 + A[1,1]*c[2,3]/60 + A[1,1]*c[3,3]/60 + A[1,2]*c[0,0]/60 + A[1,2]*c[0,1]/60 + A[1,2]*c[0,2]/60 + A[1,2]*c[0,3]/60 + A[1,2]*c[1,1]/60 + A[1,2]*c[1,2]/60 + A[1,2]*c[1,3]/60 + A[1,2]*c[2,2]/60 + A[1,2]*c[2,3]/60 + A[1,2]*c[3,3]/60
        m_loc[2,0]=m_loc[0,2]
        m_loc[0,3]=A[0,2]*c[0,0]/60 + A[0,2]*c[0,1]/60 + A[0,2]*c[0,2]/60 + A[0,2]*c[0,3]/60 + A[0,2]*c[1,1]/60 + A[0,2]*c[1,2]/60 + A[0,2]*c[1,3]/60 + A[0,2]*c[2,2]/60 + A[0,2]*c[2,3]/60 + A[0,2]*c[3,3]/60 + A[1,2]*c[0,0]/60 + A[1,2]*c[0,1]/60 + A[1,2]*c[0,2]/60 + A[1,2]*c[0,3]/60 + A[1,2]*c[1,1]/60 + A[1,2]*c[1,2]/60 + A[1,2]*c[1,3]/60 + A[1,2]*c[2,2]/60 + A[1,2]*c[2,3]/60 + A[1,2]*c[3,3]/60 + A[2,2]*c[0,0]/60 + A[2,2]*c[0,1]/60 + A[2,2]*c[0,2]/60 + A[2,2]*c[0,3]/60 + A[2,2]*c[1,1]/60 + A[2,2]*c[1,2]/60 + A[2,2]*c[1,3]/60 + A[2,2]*c[2,2]/60 + A[2,2]*c[2,3]/60 + A[2,2]*c[3,3]/60
        m_loc[3,0]=m_loc[0,3]
        m_loc[1,1]=-A[0,0]*c[0,0]/60 - A[0,0]*c[0,1]/60 - A[0,0]*c[0,2]/60 - A[0,0]*c[0,3]/60 - A[0,0]*c[1,1]/60 - A[0,0]*c[1,2]/60 - A[0,0]*c[1,3]/60 - A[0,0]*c[2,2]/60 - A[0,0]*c[2,3]/60 - A[0,0]*c[3,3]/60
        m_loc[1,2]=-A[0,1]*c[0,0]/60 - A[0,1]*c[0,1]/60 - A[0,1]*c[0,2]/60 - A[0,1]*c[0,3]/60 - A[0,1]*c[1,1]/60 - A[0,1]*c[1,2]/60 - A[0,1]*c[1,3]/60 - A[0,1]*c[2,2]/60 - A[0,1]*c[2,3]/60 - A[0,1]*c[3,3]/60
        m_loc[2,1]=m_loc[1,2]
        m_loc[1,3]=-A[0,2]*c[0,0]/60 - A[0,2]*c[0,1]/60 - A[0,2]*c[0,2]/60 - A[0,2]*c[0,3]/60 - A[0,2]*c[1,1]/60 - A[0,2]*c[1,2]/60 - A[0,2]*c[1,3]/60 - A[0,2]*c[2,2]/60 - A[0,2]*c[2,3]/60 - A[0,2]*c[3,3]/60
        m_loc[3,1]=m_loc[1,3]
        m_loc[2,2]=-A[1,1]*c[0,0]/60 - A[1,1]*c[0,1]/60 - A[1,1]*c[0,2]/60 - A[1,1]*c[0,3]/60 - A[1,1]*c[1,1]/60 - A[1,1]*c[1,2]/60 - A[1,1]*c[1,3]/60 - A[1,1]*c[2,2]/60 - A[1,1]*c[2,3]/60 - A[1,1]*c[3,3]/60
        m_loc[2,3]=-A[1,2]*c[0,0]/60 - A[1,2]*c[0,1]/60 - A[1,2]*c[0,2]/60 - A[1,2]*c[0,3]/60 - A[1,2]*c[1,1]/60 - A[1,2]*c[1,2]/60 - A[1,2]*c[1,3]/60 - A[1,2]*c[2,2]/60 - A[1,2]*c[2,3]/60 - A[1,2]*c[3,3]/60
        m_loc[3,2]=m_loc[2,3]
        m_loc[3,3]=-A[2,2]*c[0,0]/60 - A[2,2]*c[0,1]/60 - A[2,2]*c[0,2]/60 - A[2,2]*c[0,3]/60 - A[2,2]*c[1,1]/60 - A[2,2]*c[1,2]/60 - A[2,2]*c[1,3]/60 - A[2,2]*c[2,2]/60 - A[2,2]*c[2,3]/60 - A[2,2]*c[3,3]/60
        # Assmeble the entries
        m = m_loc*np.abs(np.linalg.det(J))
        
        ii[k,:] = nodes[ii_loc]
        jj[k,:] = nodes[jj_loc]
        
        mm[k,:] = m[ii_loc, jj_loc]
    
    return mm.flatten(), ii.flatten(), jj.flatten()

#%% Boundary Mass operator
def assemble_boundary_mass_operator(points, cfield, tris, lines=None):
    '''
        boundary mass operator
    '''
    x = points[:,0]
    y = points[:,1]
    z = points[:,2]
    
    N = len(x)


    
    ii = np.zeros((len(tris),3**2),dtype=int)
    jj = np.zeros((len(tris),3**2),dtype=int)
    mm = np.zeros((len(tris),3**2))
    jj_loc = np.repeat(np.r_[0:3],3)
    jj_loc = np.reshape(jj_loc,(3,3))
    
    ii_loc = np.transpose(jj_loc)
    ii_loc = ii_loc.flatten()
    jj_loc = jj_loc.flatten()
    
    m_loc = np.zeros((6,6))
    
    # Loop over all triangles
    for k, tri in enumerate(tris):
        # 6 nodes (3 corners 3 edges) for the local element
        nodes = []
        nodes += [tri[0], tri[1], tri[2]]

        nodes = np.array(nodes)        
        
        #compute c functions
        c = cfield[nodes[0:3]]           
        
        
        #local coordinate transformation        
        J = np.matrix([
            [x[tri[1]]-x[tri[0]], x[tri[2]]-x[tri[0]]], 
            [y[tri[1]]-y[tri[0]], y[tri[2]]-y[tri[0]]],
            [z[tri[1]]-z[tri[0]], z[tri[2]]-z[tri[0]]],])
        
        #surface transform
        A=np.linalg.norm(np.cross(np.transpose(J[:,0]),np.transpose(J[:,1])))
        
        #local boundary mass matrix generated by sympy
        m_loc[0,0]=c[0]/20 + c[1]/60 + c[2]/60
        m_loc[0,1]=c[0]/60 + c[1]/60 + c[2]/120
        m_loc[1,0]=m_loc[0,1]
        m_loc[0,2]=c[0]/60 + c[1]/120 + c[2]/60
        m_loc[2,0]=m_loc[0,2]
        m_loc[1,1]=c[0]/60 + c[1]/20 + c[2]/60
        m_loc[1,2]=c[0]/120 + c[1]/60 + c[2]/60
        m_loc[2,1]=m_loc[1,2]
        m_loc[2,2]=c[0]/60 + c[1]/60 + c[2]/20

        # Assmeble the entries
        m = m_loc*A
        
        ii[k,:] = nodes[ii_loc]
        jj[k,:] = nodes[jj_loc]
        
        mm[k,:] = m[ii_loc, jj_loc]
    
    return mm.flatten(), ii.flatten(), jj.flatten()


#%% Volume Source
def assemble_volume_source(points, tets, lines=None):
    '''
        volume source
    '''
    # Coordinates and number of points
    x = points[:,0]
    y = points[:,1]
    z = points[:,2]
    
    N = len(x)
    
    # Allocate the necessary space

    ii = np.zeros((len(tets),4),dtype=int)
    jj = np.zeros((len(tets),4),dtype=int)
    mm = np.zeros((len(tets),4))
    ii_loc = np.r_[0:4]
    ii_loc = ii_loc.flatten()
    m_loc=np.array([1/24, 1/24, 1/24, 1/24])

    # Loop over all tetrahedra  
    for k, tet in enumerate(tets):
        # Ten nodes (4 coreners 6 edges) for the local element
        nodes = []
        nodes += [tet[0], tet[1], tet[2], tet[3]]
        nodes = np.array(nodes)
        
        
        #local coordinate transformation        
        J = np.matrix([
            [x[tet[1]]-x[tet[0]], x[tet[2]]-x[tet[0]], x[tet[3]]-x[tet[0]]], 
            [y[tet[1]]-y[tet[0]], y[tet[2]]-y[tet[0]], y[tet[3]]-y[tet[0]]],
            [z[tet[1]]-z[tet[0]], z[tet[2]]-z[tet[0]], z[tet[3]]-z[tet[0]]]])
        
        # Assmeble the entries
        m = m_loc*np.abs(np.linalg.det(J))
        
        ii[k,:] = nodes[ii_loc]        
        mm[k,:] = m[ii_loc]
    
    return mm.flatten(), ii.flatten(), jj.flatten()

def assemble_gradient_source(points, tets, x_ref, n_ref, lines=None): 


    # Allocate the necessary space
    
    jj = np.zeros((1,4),dtype=int)
    mm = np.zeros((1,4))
    
    
    n_ref = np.matrix(n_ref)#[np.newaxis]
    
    
    idx=find_tetrahedron_containing_x(points, tets, x_ref)
    tet=tets[idx]
    
    J=np.matrix(points[tet[1:4],:]-points[tet[0],:]).T
    print(J)
    Jinv=J.I
    x_ref=np.matrix(x_ref-points[tet[0],:]).T
    x,y,z=(Jinv*x_ref)[:]       


    grad=np.matrix([[ -1, -1, -1],
[ 1, 0, 0],
[ 0, 1, 0],
[ 0, 0, 1]])
    
    
    # Assmeble the entries
    mm = grad*Jinv*n_ref.T
    
    ii = []
    ii += [tet[0], tet[1], tet[2], tet[3]]

    ii = np.array(ii)     
    mm = np.array(mm)
    
    return mm.flatten(), ii.flatten(), jj.flatten()
