#_____________________________________________________________________
# Copyright (c) 2018 Georg Mensah et al.
# All rights reserved.
#
# Last edited by: 
#
# Date: 
#
# This file is part of PyHoltz.
# PyHoltz is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Lesser General Public License as published by 
# the Free Software Foundation, version 3 of the License. 
#
# PyHoltz is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of 
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License 
# (COPYING.LESSER) along with PyHoltz.
# If not, see <https://www.gnu.org/licenses/>.
# ____________________________________________________________________
"""
Created on Tue Jun  6 17:42:12 2017

@author: mrmorningstar
"""


from copy import deepcopy
from sympy import * 
import numpy as np

x, y, z,a = symbols('x y z a')
c0, c1, c2, c3, c4, c5, c6, c7, c8, c9 = symbols('c0 c1 c2 c3 c4 c5 c6 c7 c8 c9')
J11, J12, J13, J22, J23, J33 = symbols('J11 J12 J13 J22 J23 J33')
J31, J21, J32 = J13, J12, J23 

J=Matrix([[J11, J12, J13],
       [J21, J22, J23],
       [J31, J32, J33]])

#n0, n1, n2 = symbols('n0 n1 n2')
#n=Matrix([[n0],
#          [n1],
#          [n2]])

#a = 1 - x - y - z
#suba = 1 - x - y - z
a=1-x-y-z
f0 = (2*a-1)*a
f1 = (2*x-1)*x
f2 = (2*y-1)*y
f3 = (2*z-1)*z
f4 = 4*x*a
f5 = 4*y*a
f6 = 4*z*a
f7 = 4*x*y
f8 = 4*x*z
f9 = 4*y*z



fs = [f0,f1, f2, f3, f4, f5, f6, f7, f8, f9, ]
#fs = [a,x,y,z]
cs=[ c0*a, c1*x, c2*y, c3*z]
#%%

def gradient(f):
    #return Matrix([diff(f,x),diff(f,y),diff(f,z),diff(f,a)])
    #f=f.subs(a,suba)
    return (Matrix([diff(f,x),diff(f,y),diff(f,z)])).T

def build_gradient_matrix(f):
    N=len(f)
    M=gradient(f[0])
    for i in range(1,N):
        M=M.col_insert(i,gradient(f[i]).T)
    
    return M
        
#M=build_gradient_matrix(fs)
#A=M*M.T    
    
    
#%%
c=sum(cs)
#c=1
def tet_integrate(f):
    #f=f.subs(a,suba)
    return integrate(integrate(integrate(f, (x, 0, 1 - z -y)), (y, 0, 1 - z)), (z, 0, 1))
    
def tri_integrate(f):
    #f=f.subs(a,suba)
    return integrate(integrate(f, (x, 0, 1 - y)), (y, 0, 1))


def mass_matrix(f_i,f_j):
    #f_i=f_i.subs(a,suba)
    #f_j=f_j.subs(a,suba)
    return tet_integrate(f_i*f_j)
    
def stiff_matrix(f_i,f_j):
    f_i=gradient(f_i)
    f_j=gradient(f_j)    
    #f_i=f_i.subs(a,suba)
    #f_j=f_j.subs(a,suba)    
    return -tet_integrate(c**2*f_i*J*f_j.T)[0]
    
def source_vector(f):
    #f=f.subs(a,suba)
    return tet_integrate(f)
    
def boundary_mass_marix(f_i, f_j):
    f = f_i*f_j
    return tri_integrate(c*f).subs(z, 0)
#%%    
N=len(fs)    

Md = np.zeros((N,N))
Kd = Md.copy()
Qd = np.zeros((N,1))
Bd = Md.copy()

M  = []


for i in np.arange(0,N):
    line = []
    for j in np.arange(0,N):
        line.append(0)
    M.append(line)

K = deepcopy(M)
B = deepcopy(M)
Q = deepcopy(line)
G = deepcopy(Q)

for i in range(N):
    f_i=fs[i]
    Q[i] = source_vector(f_i)
    G[i] = gradient(f_i)
    #Qd[i] = float(Q[i])
    for j in range(i,N):
        f_j=fs[j]
        
        print(i,j,flush=True)
        M[i][j] = mass_matrix(f_i,f_j)
        #Md[i,j] = float(M[i][j])
        
        K[i][j] = stiff_matrix(f_i,f_j)
        #Kd[i,j] = float(K[i][j])

        B[i][j] = boundary_mass_marix(f_i,f_j)
        #Bd[i,j] = float(B[i][j])

                
        
#Md = np.matrix(Md)
#Kd = np.matrix(Kd)
#Qd = np.matrix(Qd)
#Bd = np.matrix(Bd)



#%%

def stiff():
    for i in range(N):
        for j in range(i,N):
            #print(i,j,flush=True)
            line=str(K[i][j])
            for k in range(0,4):
                line=line.replace('c'+str(k)+'**2','c['+str(k)+','+str(k)+']')
                for l in range(0,4):
                    line=line.replace('c'+str(k)+'*c'+str(l),'c['+str(k)+','+str(l)+']')
            
            for k in range(1,4):
                for l in range(k,4):
                    line=line.replace( 'J'+str(k)+str(l) , 'A['+str(k-1)+','+str(l-1)+']' )
            line='m_loc['+str(i)+','+str(j)+']=' + line +'\n'
            yield line
            if i != j:
                yield 'm_loc['+str(j)+','+str(i)+']=m_loc['+str(i)+','+str(j)+']\n'
            #print(line)
            
def boundmass():
    if N>4:
        G=6
    else:
        G=3
    idx=[0,1,2,4,5,7]
    for i in range(G):
        for j in range(i,G):
            line=str(B[idx[i]][idx[j]])
            for k in range(0,4):
                line=line.replace('c'+str(k)+'**2','c['+str(k)+','+str(k)+']')
                line=line.replace('c'+str(k),'c['+str(k)+']')
                for l in range(0,4):
                    line=line.replace('c'+str(k)+'*c'+str(l),'c['+str(k)+','+str(l)+']')
            
            line='m_loc['+str(i)+','+str(j)+']=' + line +'\n'
            yield line
            if i != j:
                yield 'm_loc['+str(j)+','+str(i)+']=m_loc['+str(i)+','+str(j)+']\n'
    
            
def sym2code(A):
    for i in range(N):
        yield '            ['    
        for j in range(N):
            if i<=j:
                line=str(A[i][j])
            else:
                line=str(A[j][i])
            if j!=N-1:    
                yield  line +', '
            else:
                yield line+'],\n'
            
def grad2code(G):
    txt='    grad=np.matrix(['
    for i in G:
        txt+='['  
        for j in i:
            txt+=' '+str(j)+','
        txt=txt[0:-1]
        txt+='],\n'
    txt=txt[0:-2]
    txt+='])\n    '
    return txt        
            

            
#%%
if N==4:
    fid=open('operators_auto_Lin.py','w')
elif N==10:
    fid=open('operators_auto_Quad.py','w')

txt="""
from mesh_utils import smplx_finder, find_tetrahedron_containing_x
import numpy as np\n#%%Mass operator
def assemble_mass_operator(points,tets, lines=None):
    '''
        mass operator
    '''
    # Coordinates and number of points
    x = points[:,0]
    y = points[:,1]
    z = points[:,2]
    
    N = len(x)
    
    # Allocate the necessary space
"""
fid.write(txt)

fid.write('    ii = np.zeros((len(tets),'+str(N)+'**2),dtype=int)\n')
fid.write('    jj = np.zeros((len(tets),'+str(N)+'**2),dtype=int)\n')
fid.write('    mm = np.zeros((len(tets),'+str(N)+'**2))\n')

fid.write('    jj_loc = np.repeat(np.r_[0:'+str(N)+'],'+str(N)+')\n')
fid.write('    jj_loc = np.reshape(jj_loc,('+str(N)+','+str(N)+'))\n')
txt="""    ii_loc = np.transpose(jj_loc)
    ii_loc = ii_loc.flatten()
    jj_loc = jj_loc.flatten()"""
fid.write(txt)    
txt="""\n    # The constant local mass matrix
    m_loc = np.matrix(["""
fid.write(txt)
for i in sym2code(M):
    fid.write(i)

fid.write('                ])\n')

txt="""\n    # Loop over all tetrahedra  
    for k, tet in enumerate(tets):
        # Ten nodes (4 coreners 6 edges) for the local element
        nodes = []
        nodes += [tet[0], tet[1], tet[2], tet[3]]
"""
fid.write(txt)
txt="""        nodes += [smplx_finder(tet[[0,1]],lines) + N]
        nodes += [smplx_finder(tet[[0,2]],lines) + N]
        nodes += [smplx_finder(tet[[0,3]],lines) + N]
        nodes += [smplx_finder(tet[[1,2]],lines) + N]
        nodes += [smplx_finder(tet[[1,3]],lines) + N]
        nodes += [smplx_finder(tet[[2,3]],lines) + N]
"""
if N>4: fid.write(txt)
txt="""        nodes = np.array(nodes)
        
\n        #local coordinate transformation        
        J = np.matrix([
            [x[tet[1]]-x[tet[0]], x[tet[2]]-x[tet[0]], x[tet[3]]-x[tet[0]]], 
            [y[tet[1]]-y[tet[0]], y[tet[2]]-y[tet[0]], y[tet[3]]-y[tet[0]]],
            [z[tet[1]]-z[tet[0]], z[tet[2]]-z[tet[0]], z[tet[3]]-z[tet[0]]]])

        # Assmeble the entries
        m = m_loc*np.abs(np.linalg.det(J))
        
        ii[k,:] = nodes[ii_loc]
        jj[k,:] = nodes[jj_loc]
        
        mm[k,:] = m[ii_loc, jj_loc]
        
    return mm.flatten(), ii.flatten(), jj.flatten()"""
fid.write(txt)    


#stiffness
txt="""\n\n\n#%% Stiffness operator
def assemble_stiffness_operator(points, cfield, tets, lines):
    '''
        stiffness operator
    '''
    x = points[:,0]
    y = points[:,1]
    z = points[:,2]
    
    N = len(x)
"""
fid.write(txt)   


fid.write('    ii = np.zeros((len(tets),'+str(N)+'**2),dtype=int)\n')
fid.write('    jj = np.zeros((len(tets),'+str(N)+'**2),dtype=int)\n')
fid.write('    mm = np.zeros((len(tets),'+str(N)+'**2))\n')
fid.write('    jj_loc = np.repeat(np.r_[0:'+str(N)+'],'+str(N)+')\n')
fid.write('    jj_loc = np.reshape(jj_loc,('+str(N)+','+str(N)+'))\n')
txt="""    ii_loc = np.transpose(jj_loc)
    ii_loc = ii_loc.flatten()
    jj_loc = jj_loc.flatten()
    
"""
fid.write(txt)    
fid.write('    m_loc = np.zeros(('+str(N)+','+str(N)+'))\n\n')

txt="""\n    # Loop over all tetrahedra  
    for k, tet in enumerate(tets):
        # Ten nodes (4 coreners 6 edges) for the local element
        nodes = []
        nodes += [tet[0], tet[1], tet[2], tet[3]]
"""
fid.write(txt)
txt="""        nodes += [smplx_finder(tet[[0,1]],lines) + N]
        nodes += [smplx_finder(tet[[0,2]],lines) + N]
        nodes += [smplx_finder(tet[[0,3]],lines) + N]
        nodes += [smplx_finder(tet[[1,2]],lines) + N]
        nodes += [smplx_finder(tet[[1,3]],lines) + N]
        nodes += [smplx_finder(tet[[2,3]],lines) + N]
"""
if N>4: fid.write(txt)

txt="""        nodes = np.array(nodes)        
\n        #compute c**2 functions
        c = np.outer(cfield[nodes[0:4]], cfield[nodes[0:4]])           
        
\n        #local coordinate transformation        
        J = np.matrix([
            [x[tet[1]]-x[tet[0]], x[tet[2]]-x[tet[0]], x[tet[3]]-x[tet[0]]], 
            [y[tet[1]]-y[tet[0]], y[tet[2]]-y[tet[0]], y[tet[3]]-y[tet[0]]],
            [z[tet[1]]-z[tet[0]], z[tet[2]]-z[tet[0]], z[tet[3]]-z[tet[0]]]])

        A=J.I*J.I.T
        #A=A.T
\n        # Local stiffness matrix generated via Sympy
"""

fid.write(txt)

for line in stiff():
    fid.write('        '+line)

txt="""        # Assmeble the entries
        m = m_loc*np.abs(np.linalg.det(J))
        
        ii[k,:] = nodes[ii_loc]
        jj[k,:] = nodes[jj_loc]
        
        mm[k,:] = m[ii_loc, jj_loc]
        
    return mm.flatten(), ii.flatten(), jj.flatten()\n
"""
fid.write(txt)



txt="""#%% Boundary Mass operator
def assemble_boundary_mass_operator(points, cfield, tris, lines=None):
    '''
        boundary mass operator
    '''
    x = points[:,0]
    y = points[:,1]
    z = points[:,2]
    
    N = len(x)
"""

fid.write(txt)
if N>4:
    txt="""

    ii = np.zeros((len(tris),6**2),dtype=int)
    jj = np.zeros((len(tris),6**2),dtype=int)
    mm = np.zeros((len(tris),6**2))
    jj_loc = np.repeat(np.r_[0:6],6)
    jj_loc = np.reshape(jj_loc,(6,6))
    """
else:
    txt="""

    ii = np.zeros((len(tris),3**2),dtype=int)
    jj = np.zeros((len(tris),3**2),dtype=int)
    mm = np.zeros((len(tris),3**2))
    jj_loc = np.repeat(np.r_[0:3],3)
    jj_loc = np.reshape(jj_loc,(3,3))
    """
fid.write(txt)

    
txt="""
    ii_loc = np.transpose(jj_loc)
    ii_loc = ii_loc.flatten()
    jj_loc = jj_loc.flatten()

    m_loc = np.zeros((6,6))
    
    # Loop over all triangles
    for k, tri in enumerate(tris):
        # 6 nodes (3 corners 3 edges) for the local element
        nodes = []
        nodes += [tri[0], tri[1], tri[2]]
"""

fid.write(txt)

txt="""
        nodes += [smplx_finder(tri[[0,1]],lines) + N]
        nodes += [smplx_finder(tri[[0,2]],lines) + N]
        nodes += [smplx_finder(tri[[1,2]],lines) + N]
"""
if N>4: fid.write(txt)

txt="""
        nodes = np.array(nodes)        

        #compute c functions
        c = cfield[nodes[0:3]]           


        #local coordinate transformation        
        J = np.matrix([
            [x[tri[1]]-x[tri[0]], x[tri[2]]-x[tri[0]]], 
            [y[tri[1]]-y[tri[0]], y[tri[2]]-y[tri[0]]],
            [z[tri[1]]-z[tri[0]], z[tri[2]]-z[tri[0]]],])

        #surface transform
        A=np.linalg.norm(np.cross(np.transpose(J[:,0]),np.transpose(J[:,1])))

        #local boundary mass matrix generated by sympy
"""
fid.write(txt)

for line in boundmass():
    fid.write('        '+line)

txt="""
        # Assmeble the entries
        m = m_loc*A
        
        ii[k,:] = nodes[ii_loc]
        jj[k,:] = nodes[jj_loc]
        
        mm[k,:] = m[ii_loc, jj_loc]
    
    return mm.flatten(), ii.flatten(), jj.flatten()
\n
"""
fid.write(txt)

txt="""#%% Volume Source
def assemble_volume_source(points, tets, lines=None):
    '''
        volume source
    '''
    # Coordinates and number of points
    x = points[:,0]
    y = points[:,1]
    z = points[:,2]
    
    N = len(x)
    
    # Allocate the necessary space
"""
fid.write(txt)


fid.write('    ii = np.zeros((len(tets),'+str(N)+'),dtype=int)\n')
fid.write('    jj = np.zeros((len(tets),'+str(N)+'),dtype=int)\n')
fid.write('    mm = np.zeros((len(tets),'+str(N)+'))\n')
fid.write('    ii_loc = np.r_[0:'+str(N)+']\n')
fid.write('    ii_loc = ii_loc.flatten()\n')

fid.write('    m_loc=np.array('+str(Q)+')\n\n')

txt="""    # Loop over all tetrahedra  
    for k, tet in enumerate(tets):
        # Ten nodes (4 coreners 6 edges) for the local element
        nodes = []
        nodes += [tet[0], tet[1], tet[2], tet[3]]
"""
fid.write(txt)

txt="""        nodes += [smplx_finder(tet[[0,1]],lines) + N]
        nodes += [smplx_finder(tet[[0,2]],lines) + N]
        nodes += [smplx_finder(tet[[0,3]],lines) + N]
        nodes += [smplx_finder(tet[[1,2]],lines) + N]
        nodes += [smplx_finder(tet[[1,3]],lines) + N]
        nodes += [smplx_finder(tet[[2,3]],lines) + N]
"""
if N>4: fid.write(txt)
    
txt="""        nodes = np.array(nodes)


        #local coordinate transformation        
        J = np.matrix([
            [x[tet[1]]-x[tet[0]], x[tet[2]]-x[tet[0]], x[tet[3]]-x[tet[0]]], 
            [y[tet[1]]-y[tet[0]], y[tet[2]]-y[tet[0]], y[tet[3]]-y[tet[0]]],
            [z[tet[1]]-z[tet[0]], z[tet[2]]-z[tet[0]], z[tet[3]]-z[tet[0]]]])
        
        # Assmeble the entries
        m = m_loc*np.abs(np.linalg.det(J))
        
        ii[k,:] = nodes[ii_loc]        
        mm[k,:] = m[ii_loc]
    
    return mm.flatten(), ii.flatten(), jj.flatten()
"""

fid.write(txt)

txt="""
def assemble_gradient_source(points, tets, x_ref, n_ref, lines=None): 
"""
fid.write(txt)
if N==4:
    txt="""
    # Allocate the necessary space

    jj = np.zeros((1,4),dtype=int)
    mm = np.zeros((1,4))
    """
elif N==10:
        txt="""
    N=points.shape[0]
    # Allocate the necessary space

    jj = np.zeros((1,10),dtype=int)
    mm = np.zeros((1,10))
    """
    
fid.write(txt)
txt="""
    
    n_ref = np.matrix(n_ref)#[np.newaxis]


    idx=find_tetrahedron_containing_x(points, tets, x_ref)
    tet=tets[idx]
    
    J=np.matrix(points[tet[1:4],:]-points[tet[0],:]).T
    
    Jinv=J.I
    x_ref=np.matrix(x_ref-points[tet[0],:]).T
    x,y,z=(Jinv*x_ref)[:]
    x=x.item(0)
    y=y.item(0)
    z=z.item(0)        
    
"""
fid.write(txt)
fid.write(grad2code(G))
txt="""

    # Assmeble the entries
    mm = grad*Jinv*n_ref.T

    ii = []
    ii += [tet[0], tet[1], tet[2], tet[3]]
"""
fid.write(txt)
if N>4:
    txt="""
    ii += [smplx_finder(tet[[0,1]],lines) + N]
    ii += [smplx_finder(tet[[0,2]],lines) + N]
    ii += [smplx_finder(tet[[0,3]],lines) + N]
    ii += [smplx_finder(tet[[1,2]],lines) + N]
    ii += [smplx_finder(tet[[1,3]],lines) + N]
    ii += [smplx_finder(tet[[2,3]],lines) + N]
"""
    fid.write(txt)

txt="""
    ii = np.array(ii)     
    mm = np.array(mm)
    
    return mm.flatten(), ii.flatten(), jj.flatten()
"""
fid.write(txt)



fid.close()    


##%%
#fid=open('stiffness.py','w+')
#test=str(K[0][0])
#for i in range(N):
#    for j in range(i,N):
#        print(i,j,flush=True)
#        line=str(K[i][j])
#        for k in range(1,5):
#            line=line.replace('c'+str(k)+'**2','c['+str(k-1)+','+str(k-1)+']')
#            for l in range(1,5):
#                line=line.replace('c'+str(k)+'*c'+str(l),'c['+str(k-1)+','+str(l-1)+']')
#        
#        for k in range(1,4):
#            for l in range(k,4):
#                line=line.replace( 'J'+str(k)+str(l) , 'A['+str(k-1)+','+str(l-1)+']' )
#        line='m_loc['+str(i)+','+str(j)+']=' + line +'\n'
#        fid.write(line)
#        if i != j:
#            fid.write('m_loc['+str(j)+','+str(i)+']=m_loc['+str(i)+','+str(j)+']\n')
#        print(line)
#

