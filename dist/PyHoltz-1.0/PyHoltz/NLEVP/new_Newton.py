#_____________________________________________________________________
# Copyright (c) 2018 Georg Mensah et al.
# All rights reserved.
#
# Last edited by: 
#
# Date: 
#
# This file is part of PyHoltz.
# PyHoltz is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Lesser General Public License as published by 
# the Free Software Foundation, version 3 of the License. 
#
# PyHoltz is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of 
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License 
# (COPYING.LESSER) along with PyHoltz.
# If not, see <https://www.gnu.org/licenses/>.
# ____________________________________________________________________
"""
@authors: Georg Mensah, Philip Buschmann, Jonas Moeck, Alessandro Orchini TU Berlin 2015

Finds the eigenvalues (omega) and eigenvectors v of an eigenvalue problem nonlinear in the eigenvalue
L(omega) v = 0
This is solved by using a newton-type algorithm on a linear eigenvalue problem of the form
L(omega) v = lambda v 
where omega is treated as a parameter. The solutions are found when lambda = 0.   
"""

import math as math
import numpy as np
import scipy.sparse.linalg as linalg

def iteration(L,k,maxiter=10,tol=1e-16,relax=1,n_eig_val=3,v0=[],output=True,purge=False,tol_lamb=1e-8):
    ''' Utilizes Newton's method for the solution of 
    non-linear eigenvalue problems.
    
    The eigenvalue problem considered reads as follows:
    ``L(k)*v =0``
    
    Parameters
    ----------
    
    L : function, callable object
        operator family
        
    k : float or complex
        initial guess for the eigenvalue
          
    maxiter : int, optional
        maximum number of iterations (default is ``10``) 
        
    tol : float
        tolerance. Convergence is assumend  when ``abs(k_n-k_{n-1})<tol``

    relax : float, optional   
        relaxation parameter between ``0`` and ``1``. (This feature is 
        experimental, the default is ``1``, which implies no relaxation )
            
    v0: array_like, optional 
        initial guess for the eigenvector (if not specified the vector is 
        initialized usings ones only)
           
    Returns
    -------
    
    k : matrix
        eigenvalue
            
    v : matrix
        eigenvector
        
    v_adj : matrix
        adjoint eigenvector
            
    Notes
    -----
        In case of mode degenracy the vectors returned feature multiple 
        columns.

    '''
    if output: 
        print("Launching Newton...")
        
    k0=float('inf')
    iteration=0
    abs_lamb=np.zeros(n_eig_val+1) + float('inf')
    
    if v0==[]:
        v0=np.ones((L(k).shape[0],1))
        v0_adj=np.ones((L(k).shape[0],1))
        
    while (abs(k0-k)>tol or abs_lamb[n_eig_val-1]>tol_lamb) and iteration<maxiter:
        if output: 
            print("Iteration:",iteration,", Res:",abs(k0-k),
            ', Lambda:', abs_lamb, ',Freq',k/2/math.pi)
        # Update matrix
        k0=k
        A=L(k0)
                
        # Calculate n+1 eigenvalues closest to zero and sort them by distance
        lamb,v = linalg.eigs(A=A, k=n_eig_val+1, sigma = 0,v0=v0)
        index=np.argsort(abs(lamb))
        lamb=lamb[index]
        v=np.matrix(v[:,index])
        abs_lamb=abs(lamb)         

        # Calculate adjoint eigenvalues
        lamb_adj,v_adj = linalg.eigs(A=(A).H, k=n_eig_val+1, sigma = 0,v0=v0_adj)
        index_adj=np.argsort(abs(lamb_adj))
        lamb_adj=lamb_adj[index_adj]
        v_adj=np.matrix(v_adj[:,index_adj])

        # Calculate adjoint-based corrections for all n+1 eigenvalues
        k_correct = []
        for i in range(n_eig_val+1):
            deriv=(v_adj[:,i].H*L(k0,m=1)*v[:,i])/(v_adj[:,i].H*v[:,i])
            k_correct.append((relax*(k0-lamb[i]/deriv)+(1-relax)*k0).item(0))

        # Choose the closest corrected eigenvalue to perfrom the next step
        k_correct=np.array(k_correct)
        index=np.argsort(abs(k_correct-k0))
        k=k_correct[index[0]].item(0)
        
        # Go to next iteration        
        iteration+=1
        
    N_converged = min(n_eig_val,sum(abs_lamb<tol_lamb))
    if n_eig_val > sum(abs_lamb<tol_lamb):
        print("WARNING. User requested "+str(n_eig_val)+" eigenvalues but only "+str(sum(abs_lamb<tol_lamb))+" converged within tolerance.")
        print("Solver returns only those that converged. Check number of eigenvalues requested and/or tolerance.")
        print(' ')
    elif n_eig_val < sum(abs_lamb<tol_lamb):      
        print("WARNING! User requested "+str(n_eig_val)+" eigenvalues but "+str(sum(abs_lamb<tol_lamb))+" converged within tolerance.")
        print("Solver returns the number requested by the user. Check number of eigenvalues requested and/or tolerance.")
        print(' ')

    v0=v[:,index[0:N_converged]]
    v_adj=v_adj[:,index[0:N_converged]]
            
    if(iteration>=maxiter):
        print('WARNING: Newton solver exited because the maximum number of iterations was reached. Convergence is not guaranteed. Check step size and residual.')
        print(' ')

    print('################')
    print(' Newton results ')
    print('################')
    print('Number of steps:',iteration)
    print('Last step parameter variation:',abs(k0-k))
    print('Auxiliary eigenvalue lambda residual (rhs):', abs_lamb)
    print('Eigenvalue/(2*pi):',k/2/math.pi,'with (geometric) multiplicity', N_converged)
    print(' ')
    return np.array(k), np.matrix(v0), np.matrix(v_adj), iteration
        
        