#_____________________________________________________________________
# Copyright (c) 2018 Georg Mensah et al.
# All rights reserved.
#
# Last edited by: 
#
# Date: 
#
# This file is part of PyHoltz.
# PyHoltz is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Lesser General Public License as published by 
# the Free Software Foundation, version 3 of the License. 
#
# PyHoltz is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of 
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License 
# (COPYING.LESSER) along with PyHoltz.
# If not, see <https://www.gnu.org/licenses/>.
# ____________________________________________________________________
"""
Created on Mon Jun 18 16:04:32 2018

@author: saldern
"""


import PyHoltz.NLEVP.all_algebra
import PyHoltz.NLEVP.Beyn
import PyHoltz.NLEVP.Beyn_Multi
import PyHoltz.NLEVP.Halley
import PyHoltz.NLEVP.Householder
import PyHoltz.NLEVP.inverse_iter
import PyHoltz.NLEVP.new_Newton
import PyHoltz.NLEVP.Newton
import PyHoltz.NLEVP.Nicoud
import PyHoltz.NLEVP.parameter
import PyHoltz.NLEVP.perturbation_routines
import PyHoltz.NLEVP.Real_Nicoud
