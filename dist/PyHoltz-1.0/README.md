# README #

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.

### What is PyHoltz for? What does it require? ###

PyHoltz was originally developed as a framework for calculating the eigenvalues of the "non-homogenous" Helmholtz equation in complex 3D geometries, with particular focus on combustion problems.
It then evolved into a more general tool that implements state-of-the-art as well as novel techniques to solve or approximate the solutions of nonlinear eigenvalue problems.
The code requires an installation of Python3, together with standard scientific packages such as numpy, scipy, matplotlib... An installation of anaconda should be sufficient.

### How do I get the code? ###
The repository can be cloned using git or dowloaded as a zip-file from https://bitbucket.org/pyholtzdevelopers/public/downloads/ .
It is possible (optional) to install the PyHoltz package.

### How do I get set up? ###

After downloading the repository, run the script RUNME.py. This creates some data which is used by our perturbation theory algorithm. 
By default, we install data that allow the user to run perturbation theory up to 30th order. This can be changed by varying the value of the variable N in the RUNME.py file. Note that this can take a lot of time and usage disk when the data for high perturbation orders (N>40) are created.

We provide some usage examples (Python or Jupyter files) that explain some basic usage of the modules.
The examples can be found in the folder your_installation_dir/PyHoltz/Examples, and should be run from the folder your_installation_dir

### Contribution guidelines ###

This is the PyHoltz public branch. It is indended for releasing PyHoltz, but not for development. If you are interested in developing PyHoltz, please contact Georg Mensah. georg.a.mensah@tu-berlin.de

PyHoltz was developed by 
Georg A. Mensah,
Philip E. Buschmann,
Alessandro Orchini,
Jakob G. R. von Saldern,
and Jonas P. Moeck



